var NUM_COLS = 10;

var IDLE_FOLDER_PREFIX = "idleWorker_south_"
var IDLE_FILE_PREFIX = "idleWorker_south_";
var NUM_WORKER_TYPES = 4;

Pool = (function(){

	var key = {};

	function Pool(gameParam, json) {
		var $ = {
			game : gameParam,
			statName : json.stat,
			artVars : json.artVars,
			serverInfo: json.serverInfo,
			serverPositions: [],
			workerPositions: json.workerPositions,

			serverGroup : undefined,

			workerIndices: [],
			newestWorkerIndex: 0,

			monitorSprite: null,
			monitorText: null,
		};

		var targetName = $.statName == "freeWorkers" ? "hire" :
			json.stat + "Storage";
		// $.spriteHandler = new RoomSpriteHandler(game, json.artVars, targetName, json.moreInfo);

		if(!isUndefined($.serverInfo)){
			for(var i = 0; i < $.serverInfo.serverPositions.length; i++) {
				$.serverPositions.push({});
				$.serverPositions[i].x = json.serverInfo.serverPositions[i].x + json.serverInfo.serverOffsets.x; 
				$.serverPositions[i].y = json.serverInfo.serverPositions[i].y + json.serverInfo.serverOffsets.y;
			}
		}

		this._ = function(aKey) {
			if(aKey === key)
	        	return $;

	    	console.trace();
	      	throw new Error("The _() function must be called internally.");
		};
	};

	Pool.prototype = {
		create : function() {
			var $ = this._(key);

			//servers
			if(!isUndefined($.serverInfo) || this.isBreakRoom) {
				$.serverGroup = $.game.add.group();
				if(!isUndefined($.serverInfo)){
					$.serverInfo.maxDots = $.serverInfo.dotsPerServer * $.serverInfo.serverPositions.length;
				}
			}

			//monitors
			if(!isUndefined($.artVars.monitor)){
				var monitorName = "monitor_" + $.statName + "Storage";
				var monitorX = $.artVars.monitor.x;
				var monitorY = $.artVars.monitor.y;
				$.monitorSprite = $.game.add.sprite(monitorX, monitorY,
					STILL_ATLAS_NAME, monitorName);

				$.monitorText = $.game.add.text(0, 0, "0", FONT);
				centerTextWithin($.monitorText, $.monitorSprite);
				$.monitorText.y = monitorY + 5;
			}
		},

		updateResources: function(statAmounts) {
			var $ = this._(key);

			//update cubes!
			if(!isUndefined($.serverInfo)) {
				this.updateServers(statAmounts);
			}
			else if(this.isBreakRoom) {
				this.updateWorkers(statAmounts[0]);
			}

			if(!isUndefined($.artVars.monitor)){
				$.monitorText.setText(statAmounts[0].toFixed(0));
				centerTextWithin($.monitorText, $.monitorSprite);
			}
		},

		updateServers: function(statAmounts) {
			var $ = this._(key);

			var children = $.serverGroup.children;
			var spritePrefix = $.serverInfo.serverAssetPrefix + "Server_";
			var statAmount = statAmounts[0];

			if(statAmount < 0 && !isUndefined($.negativeIconName)) {
				spritePrefix = $.negativeIconName;
			}
			if(children.length > 0 && $.serverGroup.getTop().key != spritePrefix) {
				$.serverGroup.removeAll();
			}

			var numDots = Math.min($.serverInfo.maxDots,
				Math.floor(Math.abs(statAmount) / $.serverInfo.perDot));
			var fullServers = Math.floor(numDots / $.serverInfo.dotsPerServer);

			//console.log("Updating " + numDots + " " + $.statName + " dots. " + fullServers + " servers & " + remainderDots + " remainder");

			while(children.length > 0) {
				$.serverGroup.removeChild($.serverGroup.getTop());
			}

			var positions = $.serverPositions;
			for(var i = children.length; i < fullServers; i++) {
				var position = positions[i];
				var spriteName = spritePrefix + $.serverInfo.dotsPerServer;
				var sprite = $.game.add.sprite(
					position.x - $.serverGroup.x,
					position.y - $.serverGroup.y,
					STILL_ATLAS_NAME, spriteName);
				$.serverGroup.add(sprite);
			}

			//add a remainder
			if(fullServers < positions.length) {
				var remainderDots = numDots % $.serverInfo.dotsPerServer;
				var position = positions[fullServers];
				var spriteName = spritePrefix + remainderDots;
				var sprite = $.game.add.sprite(
					position.x - $.serverGroup.x,
					position.y - $.serverGroup.y,
					STILL_ATLAS_NAME, spriteName);
				$.serverGroup.add(sprite);
			}
		},

		/**
		 * Only for the employee lounge
		 **/
		updateWorkers: function(numWorkers) {
			var $ = this._(key);
			while($.serverGroup.children.length > 0 &&
				  $.serverGroup.children.length > numWorkers) {
				$.serverGroup.removeChild($.serverGroup.getTop());
				$.workerIndices.pop();
			}

			var numIcons = Math.min(numWorkers, $.workerPositions.length);
			for(var i = $.serverGroup.children.length; i < numIcons; i++) {
				var position = $.workerPositions[i];
				var workerType = Math.ceil(Math.random() * NUM_WORKER_TYPES);
				// if(i < $.workerIndices.length) {
				// 	workerType = $.workerIndices[i];
				// }
				// else {
				// 	workerType = Math.ceil(Math.random() * NUM_WORKER_TYPES);
				// }
				var spritePrefix = IDLE_FOLDER_PREFIX + workerType + "/" +
					IDLE_FILE_PREFIX + workerType + "_";
				var sprite = $.game.add.sprite(
					position.x - $.serverGroup.x,
					position.y - $.serverGroup.y,
					WORKER_ANIM_ATLAS, spritePrefix + "0");

				sprite.animations.add('working', Phaser.Animation.generateFrameNames(
						spritePrefix, 0, 30), ANIM_FPS, true);
				sprite.animations.play('working');

				$.serverGroup.add(sprite);
				$.workerIndices.push(workerType);
			}
			$.newestWorkerIndex = 0;
		},

		addWorkerType: function(workerType) {
			var $ = this._(key);
			$.workerIndices.push(workerType);
		},

		get isBreakRoom(){ return this._(key).statName == "freeWorkers"; },

		get statName(){ return this._(key).statName; },

		get backgroundTouched(){ return this._(key).backgroundTouched; },

		get newestWorkerIndex(){
			var indices = this._(key).workerIndices;
			return indices[indices.length - 1];
		},

		get serverPositions(){ return this._(key).serverInfo.serverPositions; },
		get numServers(){ return this._(key).serverGroup.children.length; },
		// set newestWorkerIndex(value) {
		// 	this._(key).newestWorkerIndex = value;
		// }
	};

	return Pool;
})();