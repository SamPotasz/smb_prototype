var GRID_ORIGIN_X = 451;
var GRID_ORIGIN_Y = 0;

var TILE_WIDTH = 13.435; //12.2;
var TILE_HEIGHT = 7.765; //7.1;

var POPUP_DURATION = 500;
var POPUP_TWEEN_HEIGHT = 7;

function prereqsMet(prereqs, stats) {
	var reqsMet = true;
	for(var i = 0; i < prereqs.length; i++) {
		var currPrereq = prereqs[i];
		var key = currPrereq.key;
		var prereqValue = currPrereq.value;
		if(stats[key] < prereqValue && prereqValue >= 0) {
			reqsMet = false;
		}
	}
	return reqsMet;
}

function pickRandomFrom(array) {
	return array[Math.floor(Math.random() * array.length)];
}

function convertToScreen(gridX, gridY, screenOffsetX, screenOffsetY){
        var world = {
        //calculate the screen coordinates
        //note: these will then be modified by the camera
        	x : screenOffsetX - (gridY * TILE_WIDTH/2) + (gridX * TILE_WIDTH/2) - (TILE_WIDTH/2),
        	y : screenOffsetY + (gridY * TILE_HEIGHT/2) + (gridX * TILE_HEIGHT/2)
        }
        return world;
}

function isUndefined(object) {
	return typeof object === "undefined";
}

function labelButton(button, game, string, style) {
	var label = game.add.text(0, 0, string.toUpperCase(), style);
	// label.addColor("white", 0);
	button.addChild(label);
	centerXWithin(label, button.width);
	centerYWithin(label, button.height);
	return label;
}

function centerTextWithin(text, sprite){
    centerXWithin(text, sprite.width);
    text.x += sprite.x;
}

function centerXWithin(displayObject, bgWidth) {
	displayObject.x = Math.round((bgWidth - displayObject.width) / 2);
}

function centerYWithin(displayObject, bgHeight) {
	displayObject.y = (bgHeight - displayObject.height) / 2;
}

function colorText(textDisplay, value) {
	var color = value >= 0 ? BLUE_TEXT : PINK_TEXT;
	textDisplay.addColor(color, 0);
}

function globalPopup(game, x, y, string) {
	var popup = this.game.add.text(x, y, string, {font: 'bold 16px Courier New'});
	var tween = game.add.tween(popup);
	tween.to({ y: y - POPUP_TWEEN_HEIGHT }, POPUP_DURATION);
	tween.onComplete.add(function(){
		popup.destroy();
	});
	tween.start();
}

function toKDollars(dollars) {
	if(Math.abs(dollars) > 1000) {
		var cashInKs = dollars / 1000.0;
		return "$" + cashInKs.toFixed(0) + "K";
	}
	else {
		return "$" + dollars.toFixed(0);
	}
}

function playSound(soundObject) {
	var sound = SOUNDS[soundObject.name];
	sound.play();
}