var START_FAST = true;

var TIME_MONITOR_X = 234;
var TIME_MONITOR_Y = 513;
var TIME_WORDS_Y = TIME_MONITOR_Y + 8;
var TIME_NUMBERS_Y = TIME_MONITOR_Y + 53;
var TIME_PIE_X = TIME_MONITOR_X + 39;
var TIME_PIE_Y = TIME_MONITOR_Y + 24;
var TIME_PIE_NAME = "HUD_timeLeft_chart/HUD_timeLeft_chart_";
var TIME_MONITOR_NAME = "HUD_timeLeft";

var CASH_MONITOR_X = 422;
var CASH_MONITOR_Y = 512;
var CASH_WORDS_Y = CASH_MONITOR_Y + 16;
var CASH_NUMBERS_Y = CASH_MONITOR_Y + 29;
var CASH_NUMBERS_X = CASH_MONITOR_X + 15;
var CASH_CHART_X = CASH_MONITOR_X + 59;
var CASH_CHART_Y = CASH_MONITOR_Y + 24;
var CASH_CHART_NAME = "businessUpgradeCashProgress/businessUpgradeCashProgress_";
var CASH_MONITOR_NAME = "HUD_cash";

var INCOME_MONITOR_X = 353;
var INCOME_MONITOR_Y = 506;
var INCOME_WORDS_Y = INCOME_MONITOR_Y + 6;
var INCOME_NUMBERS_X = INCOME_MONITOR_X + 2;
var INCOME_NUMBERS_Y = INCOME_MONITOR_Y + 23;
var INCOME_MONITOR_NAME = "HUD_income";

var CUSTOMER_MONITOR_X = 348;
var CUSTOMER_MONITOR_Y = 553;
var CUSTOMER_WORDS_Y = CUSTOMER_MONITOR_Y + 6;
var CUSTOMER_NUMBERS_X = CUSTOMER_MONITOR_X + 10;
var CUSTOMER_NUMBERS_Y = CUSTOMER_MONITOR_Y + 23;
var CUSTOMER_MONITOR_NAME = "HUD_customers";

var GRAY_TEXT = "#c8e2e8";
var BLUE_TEXT = "white";
var PINK_TEXT = "#ff6c81";
// var BLUE_TEXT = "#74c5df";

var TIME_DISPLAY_PREFIX     = "TIME LEFT";
var TARGET_DISPLAY_PREFIX   = "GOAL";
// var REVENUE_DISPLAY_PREFIX  = "EARNED: ";
var INCOME_DISPLAY_PREFIX   = "INCOME";
var CASH_DISPLAY_PREFIX = "CASH";
var CUSTOMER_DISPLAY_PREFIX = "USERS";

var LHS_STYLE = {font: '15px Maven Pro'};
var RHS_STYLE = {font: '15px Maven Pro'};

Stats = function (game, earningJson, poolsJsonParam, timingJson, _notifications) {
	this.game = game;

    this.poolsJson = poolsJsonParam;

    // this.notificationHandler = new NotificationHandler(game, _notifications);
    // this.notificationHandler.paused.add(this.onNotePaused, this);
    // this.notificationHandler.continued.add(this.onNoteContinued, this);

    this.isPaused = false;
    this.pauseSignal = new Phaser.Signal();
    this.framesPerTickSlow = timingJson.framesPerTickSlow;
    this.framesPerTickFast = timingJson.framesPerTickFast;
    this.updates = 0;
    this.ticks = 0;

    this.setConstants(earningJson);
    this.zeroVariables();

    this.pools = {};
    var numPools = this.poolsJson.length;
    for(var i = 0; i < numPools; i++) {
        var poolData = this.poolsJson[i];
        var newPool = new Pool(this.game, poolData);
        this.pools[newPool.statName] = newPool;
    }

    /** text displays */
    this.targetDisplay;
    this.timeLeftDisplay;
    this.cashDisplay;

    this.timeLeftMonitor;
    // this.cashMonitor;
    this.incomeMonitor;
    this.customersMonitor;

    this.timeLeftPie = new PieChart(this.game, TIME_PIE_X, TIME_PIE_Y, TIME_PIE_NAME);
    this.cashChart = null;
    // this.cashPie = new PieChart(this.game, CASH_PIE_X, CASH_PIE_Y, CASH_PIE_NAME);

    this.forcefield = new SecurityForcefield(game);

    this.ticked = new Phaser.Signal();
    this.gameOver = new Phaser.Signal();
    this.periodTicked = new Phaser.Signal();
    this.poolTouched = new Phaser.Signal();
}

Stats.PARTNERS_ON = true;

Stats.prototype = {

    create: function () {

        //create office displays
        this.timeLeftMonitor = this.game.add.sprite(
            TIME_MONITOR_X, TIME_MONITOR_Y, STILL_ATLAS_NAME, TIME_MONITOR_NAME);
        // this.cashMonitor = this.game.add.sprite(
        //     CASH_MONITOR_X, CASH_MONITOR_Y, STILL_ATLAS_NAME, CASH_MONITOR_NAME);
        this.incomeMonitor = this.game.add.sprite(
            INCOME_MONITOR_X, INCOME_MONITOR_Y, STILL_ATLAS_NAME, INCOME_MONITOR_NAME);
        this.customersMonitor = this.game.add.sprite(
            CUSTOMER_MONITOR_X, CUSTOMER_MONITOR_Y, STILL_ATLAS_NAME, CUSTOMER_MONITOR_NAME);

        //pie charts
        this.timeLeftPie.create();
        // this.cashPie.create();

        this.cashChart = this.game.add.sprite(
            CASH_CHART_X, CASH_CHART_Y,
            ANIM_ATLAS_NAME, CASH_CHART_NAME + "1");
        //add dots to cash chart
        // var DOT_WIDTH = 5;
        // var dotBitmap = this.game.add.bitmapData(DOT_WIDTH, DOT_WIDTH);
        // // dotBitmap.ctx.beginPath();
        // var radius = DOT_WIDTH / 2.0;
        // dotBitmap.circle(radius, radius, radius, '#ffffff');
        // var dotY = CASH_CHART_Y + this.cashChart.height / 2.0 - DOT_WIDTH;
        // var dotDrop = 3.0;
        // this.game.add.sprite(CASH_CHART_X - radius, dotY, dotBitmap);
        // var businessLevels = smb.constants.earningConstants.businessLevels;
        // for(var i = 0; i < businessLevels.length; i++) {
        //     var currLevel = businessLevels[i];
        //     var percent = currLevel / this.targetCash;
        //     // console.log("Adding dot for " + currLevel + " / " + this.targetCash + " for percent = " + percent);
        //     var dotX = percent * this.cashChart.width + CASH_CHART_X;
        //     this.game.add.sprite(dotX, dotY + percent*dotDrop, dotBitmap);
        // }
        // this.game.add.sprite(
        //     CASH_CHART_X + this.cashChart.width - radius,
        //     dotY + dotDrop, dotBitmap);

        //create prefix texts
        var timePrefix = this.game.add.text(0, TIME_WORDS_Y,
            TIME_DISPLAY_PREFIX, LHS_STYLE, this.hud);
        timePrefix.addColor(GRAY_TEXT, 0);
        // var targetPrefix = this.game.add.text(EARNED_WORDS_X, HUD_TEXT_Y,
        //     TARGET_DISPLAY_PREFIX, LHS_STYLE, this.hud);
        // targetPrefix.addColor(PINK_TEXT, 0);
        var cashPrefix = this.game.add.text(CASH_NUMBERS_X, CASH_WORDS_Y,
            CASH_DISPLAY_PREFIX, LHS_STYLE, this.hud);
        cashPrefix.addColor(GRAY_TEXT, 0);
        var incomePrefix = this.game.add.text(0, INCOME_WORDS_Y,
            INCOME_DISPLAY_PREFIX, LHS_STYLE, this.hud);
        incomePrefix.addColor(GRAY_TEXT, 0);
        var customerPrefix = this.game.add.text(0, CUSTOMER_WORDS_Y,
            CUSTOMER_DISPLAY_PREFIX, LHS_STYLE, this.hud);
        customerPrefix.addColor(GRAY_TEXT, 0);

        //create stat display texts (are updated)
        this.timeLeftDisplay =
            this.game.add.text(0, TIME_NUMBERS_Y,
                "45d, 23h", RHS_STYLE, this.hud);
        // this.targetDisplay  = this.game.add.text(RIGHT_COLUMN_X,
        //     HUD_TEXT_Y + HUD_LINE_HEIGHT * 1,
        //     "$" + this.targetCash, RHS_STYLE, this.hud);
        this.cashDisplay =
            this.game.add.text(CASH_NUMBERS_X, CASH_NUMBERS_Y,
                "$15k", RHS_STYLE, this.hud);
        // this.revenueDisplay = this.game.add.text(RIGHT_COLUMN_X,
        //     HUD_TEXT_Y + HUD_LINE_HEIGHT * 2,
        //     this.revenue, RHS_STYLE, this.hud);
        this.incomeDisplay =
            this.game.add.text(INCOME_NUMBERS_X, INCOME_NUMBERS_Y,
                this.getIncome(), RHS_STYLE, this.hud);
        this.customerDisplay = this.game.add.text(
            CUSTOMER_NUMBERS_X, CUSTOMER_NUMBERS_Y,
            this.income, RHS_STYLE, this.hud);

        this.timeLeftDisplay.addColor(BLUE_TEXT, 0);
        // this.targetDisplay.addColor(BLUE_TEXT, 0);
        this.cashDisplay.addColor(BLUE_TEXT, 0);
        // this.revenueDisplay.addColor(PINK_TEXT, 0);
        this.incomeDisplay.addColor(BLUE_TEXT, 0);
        this.customerDisplay.addColor(BLUE_TEXT, 0);

        centerTextWithin(timePrefix, this.timeLeftMonitor);
        centerTextWithin(this.timeLeftDisplay, this.timeLeftMonitor);
        // centerTextWithin(cashPrefix, this.cashMonitor);
        // centerTextWithin(this.cashDisplay, this.cashMonitor);
        centerTextWithin(incomePrefix, this.incomeMonitor);
        centerTextWithin(customerPrefix, this.customersMonitor);
        // centerTextWithin(this.incomeDisplay, this.incomeMonitor);

        //for debugging purposes
        // this.revenueDisplay = this.game.add.text(10, 670, "REVENUE: " + this.revenue, FONT);

        var numPools = this.pools.length;
        for(var key in this.pools) {
            var currPool = this.pools[key];
            currPool.create();
        }

        this.forcefield.create(
            this.pools.emails.serverPositions,
            this.pools.credentials.serverPositions,
            this.pools.creditCards.serverPositions);

        // this.notificationHandler.create();

        this.updateDisplays();
    },

    setConstants: function(json) {
        this.startingCash = json.startingCash;
        this.cash = this.startingCash;
        this.targetCash = json.targetCash;
        this.debtLimit = -20000; //json.debtLimit;
        this.targetDuration = json.targetDuration;
        this.timeLeft = this.targetDuration;
        this.dollarsPerEmail = json.dollarsPerEmail;
        this.ticksPerPayPeriod = json.ticksPerPayPeriod;

        this.startingWorkers = json.startingWorkers;
        this.numWorkers = this.startingWorkers;
        this.freeWorkers = this.numWorkers;
        this.salary = json.salary;
        // this.salary = 1000;

        this.analyticsDollars = json.analyticsDollars;
    },

    zeroVariables: function() {
        this.income = 0;
        this.revenue = 0;
        this.efficiency = 1.0;
        this.productScore = 0;
        this.emails = 0;
        this.credentials = 0;
        this.creditCards = 0;

        this.totalSalaryPaid = 0;
        this.spentOnUpgrades = 0;

        this.oldData = {
            "emails" : 0,
            "credentials" : 0,
            "creditCards" : 0,
        };

        this.partnerData = {};
        this.partnerData.emails = 0;
        this.partnerData.credentials = 0;
        this.partnerData.creditCards = 0;
        this.press = 1.0;

        this.employeeSecurity = 0;
        this.dataSecurity = 0;
        this.responsePlan = 0;
        this.updatesPerTick = START_FAST ? this.framesPerTickFast : this.framesPerTickSlow; //UPDATE_PER_TICK_FAST;

        this.employeeSecurityIsOn = false;
        this.dataSecurityIsOn = false;
    },

    onGameRestart: function() {
        this.cash = this.startingCash;
        this.zeroVariables();
        this.timeLeft = this.targetDuration;
        this.numWorkers = this.startingWorkers;
        this.freeWorkers = this.numWorkers;

        this.updateDisplays();
        this.isPaused = false;
        console.log("Game restart (in stats)");
        this.pauseSignal.dispatch(this.isPaused);
    },

    update: function () {
        if(!this.isPaused) {
            if(this.timeLeft > 0) {
                this.updates++;
                if (this.updates % this.updatesPerTick == 0) {
                    // this.onTick();
                    this.ticked.dispatch();
                }
            }
            else {  //gameover
                var youWin = this.cash >= this.targetCash;
                this.gameOver.dispatch(youWin);
                this.isPaused = true;
                console.log("gameover in stats");
                this.pauseSignal.dispatch(this.isPaused);
            }
        }
    },

    onTicked: function () {
        this.ticks++;
        this.timeLeft--;

        var hours = Math.floor(this.timeLeft / 24);
        var minutes = this.timeLeft % 24;

        // this.timeLeftDisplay.setText(this.timeLeft);
        this.timeLeftDisplay.setText(hours + "d, " + minutes + "h");
        var percentDone = this.ticks / this.targetDuration;
        this.timeLeftPie.displayPercentage(percentDone);

        if(this.ticks % this.ticksPerPayPeriod == 0) {
            this.incorporateIncome();
            if(this.cash < this.debtLimit) {
                this.gameOver.dispatch(false);
                this.isPaused = true;
                console.log("Game over (debt limit)");
                this.pauseSignal.dispatch(this.isPaused);
            }
            this.periodTicked.dispatch();
        }

        this.updateDisplays();
        // this.notificationHandler.onTicked(this);
    },

    incorporateIncome: function() {
        var income = this.getIncome();
        this.cash += income;
        //don't include payroll in revenue
        this.revenue += this.income;

        var payroll = (this.numWorkers - 1) * this.salary;
        this.totalSalaryPaid += payroll;
    },

    //get income includes payroll and accrued income
    getIncome: function() {
        var payroll = (this.numWorkers - 1) * this.salary;
        return this.income - payroll;
    },

    onBreached: function() {
        this.isPaused = true;
        this.pauseSignal.dispatch(this.isPaused);
    },

    onContinued: function() {
        this.isPaused = false;
        this.pauseSignal.dispatch(this.isPaused);
    },

    onProjectStart: function(project) {
        this.cash -= project.cost;
    },

    //added to project
    onWorkerAdded: function(){
        this.freeWorkers--;
        this.pools.freeWorkers.updateResources([this.freeWorkers]);
    },
    //removed from project, added to lounge
    onWorkerRemoved: function(workerType) {
        this.freeWorkers++;
        this.pools.freeWorkers.addWorkerType(workerType);
        this.pools.freeWorkers.updateResources([this.freeWorkers]);
    },

    updateContinuousValue: function(project, value) {
        // console.log(project);
        for(var name in project.effects) {
            this[name] = value;
        }

        this.updateDisplays();
    },

    updateMoneyDisplays: function() {
        var cashInKs = Math.floor(this.cash / 1000.0);
        // var targetInKs = this.targetCash / 1000.0;
        this.cashDisplay.setText("$" + cashInKs.toFixed(0) + "K");// / " + targetInKs + "K");
        colorText(this.cashDisplay, this.cash);
        // this.revenueDisplay.setText("REVENUE: " + this.revenue.toFixed(2));

        // this.cashPie.displayPercentage(Math.max(0, this.cash / this.targetCash));
        var cashPercentage = (Math.max(0, this.cash / this.targetCash) * 100).toFixed(0);
        var cashFrame = CASH_CHART_NAME + cashPercentage;
        // console.log("cashFrame: " + cashFrame);
        this.cashChart.frameName = cashFrame;
    },
    updateIncomeDisplay:function(projectIncome, partnerIncome) {
        // console.log("updating incomeDisplay with " + projectIncome);
        // var toDisplay = (this.getIncome() + projectIncome + partnerIncome).toFixed(0);
        var toDisplay = this.getIncome() + projectIncome + partnerIncome;
        this.incomeDisplay.setText(toKDollars(toDisplay));
        colorText(this.incomeDisplay, toDisplay);
    },

    incrementProductScore: function() {
        this.addEffects({"productScore": 1});
    },

    onUpgradeStarted: function(cost) {
        this.cash -= cost;
        this.spentOnUpgrades += cost;
    },

    //add stats from the finished project to the game's state
    addEffects: function(effects, project) {
        //true if effects come from project. false if from a partner
        var isProject = !isUndefined(project);

        var popupString = "";

        popupString += this.addProperty(effects, "productScore", "Product Development");
        // this.addProperty(project, "efficiency");

        var newRevenue = 0;
        if(effects["cash"] > 0) {
            newRevenue = effects["cash"] * this.efficiency;
        }
        //add cash from advertising
        if(effects["advertising"] > 0){
            newRevenue = this.dollarsPerEmail * this.totalEmails;// * this.press ;
        }
        if(newRevenue > 0) {
            this.cash += newRevenue;
            this.revenue += newRevenue;
            popupString += "\n+$" + newRevenue.toFixed(2);
        }

        popupString += this.addProperty(effects, "numWorkers", "Employee");
        if(!(typeof effects.numWorkers === "undefined")) {
            this.freeWorkers += effects.numWorkers;
            var soundEffect = effects.numWorkers > 0 ? SOUND_HIRED : SOUND_FIRED;
            playSound(soundEffect);
        }

        if(effects.income > 0) {
            var adding = effects.income;// *= this.press;
            this.income += adding;
            if(adding > 0) {
                popupString += "\n+" + adding + " income";
            }
        }

        if(effects.press > 0) {
            var pressMoney = 0;
            pressMoney += (this.emails + this.partnerData.emails) * this.analyticsDollars.emails;
            pressMoney += (this.credentials + this.partnerData.credentials) * this.analyticsDollars.credentials;
            pressMoney += (this.creditCards + this.partnerData.creditCards) * this.analyticsDollars.creditCards;
            if(pressMoney > 0) {
                popupString += "\n+$" + pressMoney;
            }
        }

        // popupString += this.addProperty(effects, "press", "Analytics");
        popupString += this.addProperty(effects, "employeeSecurity", "Employee Security");
        popupString += this.addProperty(effects, "dataSecurity", "Data Security");

        /**
         *  differentiate b/w internal and external data
         */
        if(isProject) {
            popupString += this.addProperty(effects, "emails", "Emails");
            popupString += this.addProperty(effects, "credentials", "Passwords");
            popupString += this.addProperty(effects, "creditCards", "Credit Cards");
        }
        else {
            this.addPartnerProperty(effects, "emails");
            this.addPartnerProperty(effects, "credentials");
            this.addPartnerProperty(effects, "creditCards");
        }

        if(isProject) {
            project.showPopup(popupString);
        }
    },

    addProperty: function(effects, propertyName, displayString) {
        var popupString = "";
        if(effects[propertyName] !== undefined) {
            var adding = effects[propertyName];
            this[propertyName] += adding;
            if(adding > 0) {
                popupString += "\n+" + adding.toFixed(0) + " " + displayString;
            }
        }
        // console.log("adding " + popupString);
        return popupString;
    },

    addPartnerProperty: function(effects, propertyName) {
        if(!(typeof effects[propertyName] === "undefined")) {
            var adding = effects[propertyName];
            this.partnerData[propertyName] += adding;
        }
    },

    updateDisplays: function() {
        this.updateMoneyDisplays();
        this.updatePoolDisplays();

        this.customerDisplay.setText(this.income);

        this.forcefield.updateDisplay(this.dataSecurity,
            this.pools.emails.numServers,
            this.pools.credentials.numServers,
            this.pools.creditCards.numServers);
    },

    updatePoolDisplays: function() {
        this.pools.creditCards.updateResources([this.creditCards, this.oldData.creditCards]);
        this.pools.credentials.updateResources([this.credentials, this.oldData.credentials]);
        this.pools.emails.updateResources([this.emails, this.oldData.emails]);
        // this.pools.cash.updateResources([this.cash]);
        // this.pools.income.updateResources([this.income]);
        this.pools.freeWorkers.updateResources([this.freeWorkers]);
    },

    getSecurityString: function(isOn, strength) {
        // console.log("getting string for " + isOn + " and " + strength);
        var middle = isOn ? "On" : "Off";
        var suffix;
        switch(strength) {
            case 0:
                suffix = "None";
                break;
            case 1:
                suffix = "Low";
                break;
            case 2:
                suffix = "Medium";
                break;
            case 3:
                suffix = "High";
                break;
        }
        return middle + " & " + suffix;
    },

    onFastForward: function() {
        this.updatesPerTick = this.framesPerTickFast; //UPDATE_PER_TICK_FAST;
    },
    onPlay: function() {
        this.updatesPerTick = this.framesPerTickSlow; // UPDATE_PER_TICK;
    },

    /*
    onPoolBGTouched: function(statName) {
        this.poolTouched.dispatch(statName);
    },

    hidePoolPopups: function(exception) {
        // console.log("Hiding pool popups because " + exception + " was clicked.");
        for(var poolKey in this.pools) {
            var currPool = this.pools[poolKey];
            if(currPool.statName != exception){
                if(!currPool.isBreakRoom){
                    currPool.spriteHandler.hidePopup();
                }
            }
        }
    },
    */

    onNotePaused: function() {
        this.isPaused = true;
        this.pauseSignal.dispatch(this.isPaused);
    },

    onNoteContinued: function() {
        this.isPaused = false;
        this.pauseSignal.dispatch(this.isPaused);
    },

    get totalEmails() {
        return this.emails + this.partnerData.emails;
    },
    get totalCredentials() {
        return this.credentials + this.partnerData.credentials;
    },
    get totalCreditCards() {
        return this.creditCards + this.partnerData.creditCards;
    },
    get totalData() {
        return this.totalEmails + this.totalCredentials + this.totalCreditCards;
    },

    get profit() {
        return this.cash - this.startingCash;
    },

    /*
    set inputEnabled(value) {
        for(var key in this.pools) {
            if (!this.pools.hasOwnProperty(key)) {
                //The current property is not a direct property of p
                continue;
            }
            this.pools[key].inputEnabled = value;
        }
    },
    */

    get newestWorkerIndex() {
        return this.pools.freeWorkers.newestWorkerIndex;
    },

    // get poolTouched(){ this.poolTouched; },
};