WorkTimer = (function(){

	var key = {};

	function WorkTimer(maxWorkersParam, maxTimeParam) {

		var $ = {
			initialWorkers : maxWorkersParam,
			maxWorkers : maxWorkersParam,
			numWorkers : 0,
			maxTime : maxTimeParam,
			timeLeft : maxTimeParam,
			isCompleted : false,
			started : new Phaser.Signal(),
		}

		this._ = function(aKey) {
			if(aKey === key)
	        	return $;

	    	console.trace();
	      	throw new Error("The _() function must be called internally.");
		}
	}

	//public members!
	WorkTimer.prototype = {
		get maxWorkers() { return this._(key).maxWorkers; },
		set maxWorkers(value) { this._(key).maxWorkers = value; },

		get numWorkers() { return this._(key).numWorkers; },
		set numWorkers(value) { this._(key).numWorkers = value; },

		incrementWorkers : function() {
			this.numWorkers = this.numWorkers + 1;
		},
		decrementWorkers : function() {
			this.numWorkers = Math.max(0, this.numWorkers - 1);
		},

		increaseMaxWorkers : function(newWorkers) {
			this.maxWorkers = this.maxWorkers + newWorkers;
		},

		get maxTime() { return this._(key).maxTime; },
		set maxTime(value) {
			this._(key).maxTime = value;
			this._(key).timeLeft = value;
		},

		get isCompleted() { return this._(key).isCompleted; },

		get started() { return this._(key).started; },

		get timeLeft(){
			return this._(key).timeLeft;
		},

		get canAddWorker() {
			if(this.maxWorkers == 0) {
				return this.numWorkers == 0;
			}
			else {
				return this.numWorkers < this.maxWorkers;
			}
		},

		resetTimer : function(){
			var $ = this._(key);
			$.timeLeft = $.maxTime;
			$.isCompleted = false;
		},

		onTicked : function() {
			if(this.numWorkers > 0) {
				var timeLeft = this.timeLeft;
				this._(key).timeLeft = this.timeLeft - this.numWorkers;
				if(this.timeLeft <= 0) {
					this._(key).isCompleted = true;
				}
			}
		},

		onCompleted : function() {
			this._(key).timeLeft = this.timeLeft + this.maxTime;
			this._(key).isCompleted = false;
		},

		onGameRestart: function() {
			var $ = this._(key);
			$.maxWorkers = $.initialWorkers;
			$.numWorkers = 0;
			$.timeLeft = $.maxTime;
			$.isCompleted = false;
		},
	};
	return WorkTimer;
})();