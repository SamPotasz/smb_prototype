var MAX_ICONS = 8;

var DESK_FULL_FOLDER_PRE = "deskFull_"
var FULL_FRAME_PREFIX = "deskFull_";
var FULL_FRAME_SUFFIX = "_1_1";
var EMPTY_FRAME_PREFIX = "deskEmpty_";

WorkersDisplay = (function(){

	var key = {};

	function WorkersDisplay(gameParam, deskVarsParam, nameParam) {
		var $ = {
			game : gameParam,
			workerSpriteIndices : [],
			workersGroup : undefined,
			emptiesGroup : undefined,

			//group that's displayed when maxWorkers is too high
			bigNumGroup: undefined,
			textDisplay: undefined,

			name: nameParam,

			deskDirection: deskVarsParam.direction,
			deskPositions: deskVarsParam.deskPositions,
		};

		this._ = function(aKey) {
			if(aKey === key)
	        	return $;

	    	console.trace();
	      	throw new Error("The _() function must be called internally.");
		}
	}

	WorkersDisplay.prototype = {
		create: function(workersScreen, maxWorkers) {
			var $ = this._(key);

			$.emptiesGroup = $.game.add.group();
			$.emptiesGroup.x = workersScreen.x;
			$.emptiesGroup.y = workersScreen.y;

			$.workersGroup = $.game.add.group();
			$.workersGroup.x = workersScreen.x;
			$.workersGroup.y = workersScreen.y;

			$.bigNumGroup = $.game.add.group();
			$.bigNumGroup.x = workersScreen.x + 40;
			$.bigNumGroup.y = workersScreen.y + 100;
			$.textDisplay = $.game.add.text(55, 10, "", FONT);
			var employeeSprite = $.game.add.sprite(0, 0, WORKER_ANIM_ATLAS, 'deskFull_east_1/deskFull_east_1_1');
			$.bigNumGroup.add(employeeSprite);
			$.bigNumGroup.add($.textDisplay);

			this.updateDisplay(0, maxWorkers, 1);
		},

		updateDisplay: function(numWorkers, maxWorkers, workerSpriteIndex) {
			numWorkers = Math.min(numWorkers, maxWorkers);

			var $ = this._(key);

			var tooManyWorkers = maxWorkers > MAX_ICONS ||
				maxWorkers > $.deskPositions.length;
			if(!tooManyWorkers) {
				var deskDir = $.deskDirection.toLowerCase();

				//add full desks
				var oldNumWorkers = $.workerSpriteIndices.length;
				while($.workerSpriteIndices.length > numWorkers) {
					// var toRemove = $.workerSpriteIndices.pop();
					var toRemove = $.workersGroup.getTop();
					$.workersGroup.remove(toRemove);

					$.workerSpriteIndices.pop();
				}
				while($.workerSpriteIndices.length < numWorkers) {
					//FOR PLAYTEST, JUST RANDOMIZE THE WORKER TYPE
					workerSpriteIndex = Math.ceil(Math.random() * NUM_WORKER_TYPES);
					var fullPrefix = DESK_FULL_FOLDER_PRE + deskDir + "_" + workerSpriteIndex + "/" +
					FULL_FRAME_PREFIX + deskDir + "_" + workerSpriteIndex + "_";

					var positionIndex = $.workerSpriteIndices.length;
					var workerX = $.deskPositions[positionIndex].x - $.workersGroup.x;
					var workerY = $.deskPositions[positionIndex].y - $.workersGroup.y;
					var worker = $.game.add.sprite(workerX, workerY, WORKER_ANIM_ATLAS, fullPrefix + "0");

					worker.animations.add('working', Phaser.Animation.generateFrameNames(
						fullPrefix, 0, 20), ANIM_FPS, true);
					worker.animations.play('working');

					$.workerSpriteIndices.push(workerSpriteIndex);
					$.workersGroup.add(worker);
				}

				//add empty desks
				var numEmpties = maxWorkers - numWorkers;
				var emptyName = EMPTY_FRAME_PREFIX + $.deskDirection.toLowerCase();
				while($.emptiesGroup.children.length > numEmpties) {
					$.emptiesGroup.remove($.emptiesGroup.getTop());
				}
				for(var i = 0; i < numEmpties; i++) {
					var index = i + numWorkers;
					var emptyX = $.deskPositions[index].x - $.emptiesGroup.x;// + spriteOffsetX;
					var emptyY = $.deskPositions[index].y - $.emptiesGroup.y;// + spriteOffsetY;
					var deskSprite = $.game.add.sprite(emptyX, emptyY, STILL_ATLAS_NAME, emptyName);
					$.emptiesGroup.add(deskSprite);
				}
			}

			//update the text display that we show if maxWorkers is big enough
			$.textDisplay.setText(numWorkers + " / " + maxWorkers);
			$.bigNumGroup.visible = tooManyWorkers;
			$.workersGroup.visible = !tooManyWorkers;
			$.emptiesGroup.visible = !tooManyWorkers;
		},

		showLocked: function(locked) {
			var $ = this._(key);
			if(locked) {
				$.emptiesGroup.alpha = 0.0;
				$.workersGroup.alpha = 0.0;
				$.bigNumGroup.alpha = 0.0;
			}
			else {
				$.emptiesGroup.alpha = 1.0;
				$.workersGroup.alpha = 1.0;
				$.bigNumGroup.alpha = 1.0;
			}
		},

		get lastWorkerType(){
			var $ = this._(key);
			return $.workerSpriteIndices[$.workerSpriteIndices.length - 1];
		},
		get workerSpriteIndices(){ return this._(key).workerSpriteIndices; },
	};

	return WorkersDisplay;
})();
