var TICKS_PER_ATTACK = 7;

var ATTACK_SPRITE = "attack";
var ATTACK_START_POS = {x: 278, y: -10};
var ATTACK_END_POS = {x: 278, y: 56};
var ATTACK_DURATION = 250;

AttackVisualizer = (function(){

	var key = {};

	function AttackVisualizer(gameParam) {
		var $ = {
			game: gameParam,
			numTicks: 0,
		};

		this._ = function(aKey) {
			if(aKey === key)
	        	return $;

	    	console.trace();
	      	throw new Error("The _() function must be called internally.");
		}
	}

	AttackVisualizer.prototype = {
		onTicked: function() {
			var $ = this._(key);
			$.numTicks++;
			if($.numTicks % TICKS_PER_ATTACK == 0) {
				//add attack
				var sprite = $.game.add.sprite(ATTACK_START_POS.x, ATTACK_START_POS.y,
					STILL_ATLAS_NAME, ATTACK_SPRITE);
				var tween = $.game.make.tween(sprite);
				tween.to(ATTACK_END_POS, ATTACK_DURATION);
				tween.onComplete.add(function(){
					sprite.destroy();
				});
				tween.start();
			}
		},

		onGameRestart: function() {
			var $ = this._(key);
			$.numTicks = 0;
		}
	};

	return AttackVisualizer;
})();
