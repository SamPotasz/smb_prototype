var BREACH_DEBUG_ON = true;
var BREACH_START_ON = true;

var MAX_PRODUCT_SCORE = 4;

var BREACH_BACKGROUND_X = 169;
var BREACH_BACKGROUND_Y = 112;
var BREACH_BG_WIDTH = 339;
var BREACH_BG_HEIGHT = 410;
var FREQUENCY_START = 100;

var SWITCH_X = 880;
var SWITCH_Y = 500;

var DEBUG_X = 880; //710;
var DEBUG_Y = 120; //480;

var ALERT_X = 260;
var ALERT_Y = 220;
var ALERT_SPRITE_NAME = "breach";

var TEXT_X = 10;
var TEXT_Y = 10;

var TYPE_TEXT_Y = 45;
var SUMMARY_TEXT_Y = 135;
var WHAT_LOST_Y = 202;
var EMAIL_LOST_Y = 230;
var PASSWORD_LOST_Y = 250;
var CREDIT_LOST_Y = 270;
var FINES_Y = 310;
var CUSTOMERS_LOST_Y = 290;
var TOTAL_LOST_Y = 340;

var CONTINUE_X = 300;
var CONTINUE_Y = 370;

var WORD_WRAP_WIDTH = BREACH_BG_WIDTH - TEXT_X * 2;
var LINE_SPACING_BREACH = -8;

//how many times we automatically show the popup on breach
var NUM_SHOW_MODAL = 3;

var BREACH_TITLE_STYLE = {font: '22px Maven Pro', fill: 'white', stroke: 'white'};
var BREACH_BODY_STYLE = {font: '14px Maven Pro', fill: 'white', stroke: 'white'};
var DEBUG_STYLE = {font: '12px Courier New'};

var BreachTypeEnum = Object.freeze({
	INTERNAL: "Employee error breach",
	EXTERNAL: "Malicious attack on your data",
	THIRD_PARTY: "One of your partners was breached"
});

var BREACH_ALERT_POSITIONS = {
	"external": {"x": 388, "y": 100},
	"internal": {"x": 85, "y": 434},
	"thirdParty": {"x": 787, "y": 155}
};

var BREACH_CATEGORIES = ["minimal", "low", "medium", "high"];

BreachController = function(game, stats, busLevelHandler, constants) {
	this.game = game;
	this.stats = stats;
	this.levelHandler = busLevelHandler;

	this.breachesOn = BREACH_START_ON;
	this.timesBreached = 0;
	this.totalCashLost = 0;
	this.totalCustomersLost = 0;

	//set strength variables
	this.maxStrength = constants.strength.maxValue;
	this.threshold = {};
	this.threshold.high = constants.strength.highThreshold;
	this.threshold.low = constants.strength.lowThreshold;
	this.threshold.minimal = constants.strength.minThreshold;
	// this.strengthDecrements = constants.strength.decrements;
	// console.log("Training decrement: " + this.strengthDecrements.training);

	this.damages = constants.damage;
	// console.log(this.damages.medium);
	// console.log(this.damages.dollarsPerRecord);

	//how many ticks between breaches

	this.maxPeriod = constants.period.maxValue;
	this.minPeriod = constants.period.minValue;
	//set up three timers for the three breaches
	this.periods = {};
	this.ticksLeft = {};
	for(var breachType in constants.period) {
		var currPd = constants.period[breachType];
		this.periods[breachType] = {
			maxValue: currPd.maxValue,
			minValue: currPd.minValue,
			currValue: currPd.maxValue
		};
		this.ticksLeft[breachType] = currPd.maxValue;
	};

	this.ticksLeft.internal *= 1.5;
	//FOR DEBUGGING PURPOSES ONLY
	// this.ticksLeft.external = 20;
	// this.ticksLeft.internal = 50;

	//how much we decrement period by per stat
	this.periods.decrements = {};
	for(name in constants.decrements) {
		this.periods.decrements[name] = constants.decrements[name];
		console.log(name + ": " + this.periods.decrements[name]);
	}

	this.numPartners = 0;
	this.partnerDefense = 0;

	this.attackVisualizer = new AttackVisualizer(this.game);

	this.breached = new Phaser.Signal();
	this.continued = new Phaser.Signal();
};

BreachController.TYPE_TEXT_STRINGS = {
	"internal": "An employee error has lead to a breach. Are they trained well enough?",
	"external": "Data thieves have broken into your database! Have you invested in security?",
	"thirdParty": "One of your partners has been breached, and now you\'re compromised as well! How much do you know about their security practices?"
};

BreachController.prototype = {
	create: function() {
		this.group = this.game.add.group();

		// create a new bitmap data object
	    var bmd = this.game.add.bitmapData(BREACH_BG_WIDTH, BREACH_BG_HEIGHT);

	    // draw to the canvas context like normal
	    bmd.ctx.beginPath();
	    bmd.ctx.rect(0,0,BREACH_BG_WIDTH,BREACH_BG_HEIGHT);
	    bmd.ctx.fillStyle = GRAY_BG_COLOR;
	    bmd.ctx.fill();

	    // use the bitmap data as the texture for the sprite
	    this.sprite = this.game.add.sprite(0, 0, bmd);
		this.continueButton = this.game.add.button(CONTINUE_X, CONTINUE_Y, STILL_ATLAS_NAME, this.onContinueClick, this, 
			'buttonPopup','buttonPopup','buttonPopup','buttonPopup');
		labelButton(this.continueButton, this.game, "CONTINUE", BREACH_BODY_STYLE);
		centerXWithin(this.continueButton, BREACH_BG_WIDTH);

		var titleText = this.game.add.text(0, TEXT_Y, "YOU'VE BEEN BREACHED!", BREACH_TITLE_STYLE);
		centerXWithin(titleText, BREACH_BG_WIDTH);
		this.typeText = 	this.game.add.text(TEXT_X, TYPE_TEXT_Y, "", BREACH_BODY_STYLE);
		this.typeText.wordWrap = true;
		this.typeText.wordWrapWidth = WORD_WRAP_WIDTH;
		this.typeText.lineSpacing = LINE_SPACING_BREACH;

		this.strengthText = this.game.add.text(TEXT_X, SUMMARY_TEXT_Y, "", BREACH_BODY_STYLE);
		this.strengthText.lineSpacing = LINE_SPACING_BREACH;

		var whatLostText = this.game.add.text(TEXT_X, WHAT_LOST_Y, "Here's what we lost:", BREACH_BODY_STYLE);

		this.texts = {};
		this.texts.emails = this.game.add.text(TEXT_X, EMAIL_LOST_Y, "", BREACH_BODY_STYLE);
		this.texts.credentials = this.game.add.text(TEXT_X, PASSWORD_LOST_Y, "", BREACH_BODY_STYLE);
		this.texts.creditCards = this.game.add.text(TEXT_X, CREDIT_LOST_Y, "", BREACH_BODY_STYLE);
		this.texts.cash = this.game.add.text(TEXT_X, FINES_Y, "", BREACH_BODY_STYLE);
		this.texts.customers = this.game.add.text(TEXT_X, CUSTOMERS_LOST_Y, "", BREACH_BODY_STYLE);
		this.texts.totals = this.game.add.text(TEXT_X, TOTAL_LOST_Y, "", BREACH_BODY_STYLE);


		this.group.add(this.sprite);
		this.group.add(titleText);
		this.group.add(this.continueButton);
		this.group.add(this.typeText);
		this.group.add(this.strengthText);
		this.group.add(whatLostText);
		this.group.add(this.texts.emails);
		this.group.add(this.texts.credentials);
		this.group.add(this.texts.creditCards);
		this.group.add(this.texts.cash);
		this.group.add(this.texts.totals);
		this.group.add(this.texts.customers);
		centerXWithin(this.group, GAME_WIDTH);
		centerYWithin(this.group, GAME_HEIGHT);
		this.group.visible = false;
		this.game.world.removeChild(this.group);

		//add button to toggle breaches
		/*
		this.game.add.button(SWITCH_X, SWITCH_Y, STILL_ATLAS_NAME, this.onSwitchClick, this, 'buttonPopup');
		this.switchText = this.game.add.text(SWITCH_X + 5, SWITCH_Y + 5,
			"Breaches:\nON", DEBUG_STYLE);

		this.alertSprite = this.game.add.button(ALERT_X, ALERT_Y, STILL_ATLAS_NAME, this.onAlertClick, this,
			ALERT_SPRITE_NAME, ALERT_SPRITE_NAME, ALERT_SPRITE_NAME, ALERT_SPRITE_NAME);
		this.game.world.removeChild(this.alertSprite);

		//debug group
		this.debugGroup = this.game.add.group();
		this.debugGroup.x = DEBUG_X;
		this.debugGroup.y = DEBUG_Y;

		breachDebug = this.game.add.text(0, 0, "BREACH DEBUG", DEBUG_STYLE);
		this.ticksDisplay = {};
		this.periodDisplay = {};
		this.ticksDisplay.internal = this.game.add.text(0, 17, "Ticks left: " + this.ticksLeft.internal, DEBUG_STYLE);
		this.periodDisplay.internal = this.game.add.text(0, 17*2, "Internal pd:  " + this.periods.internal.currValue, DEBUG_STYLE);
		this.ticksDisplay.external = this.game.add.text(0, 17*3 + 5, "Ticks left: " + this.ticksLeft.external, DEBUG_STYLE);
		this.periodDisplay.external = this.game.add.text(0, 17*4 + 5, "External pd:  " + this.periods.external.currValue, DEBUG_STYLE);
		this.ticksDisplay.thirdParty = this.game.add.text(0, 17*5 + 10, "Ticks left: " + this.ticksLeft.thirdParty, DEBUG_STYLE);
		this.periodDisplay.thirdParty = this.game.add.text(0, 17*6 + 10, "thirdParty pd:  " + this.periods.thirdParty.currValue, DEBUG_STYLE);

		this.debugCash = this.game.add.text(0, 17*7 + 15, "Cash Lost:  " + this.totalCashLost, DEBUG_STYLE);
		this.debugCustomers = this.game.add.text(0, 17*8 + 15, "Customers Lost:  " + this.totalCustomersLost, DEBUG_STYLE);

		this.debugEmpSec = this.game.add.text(0, 17*10, "EmpSec: " + this.calculateDefense("internal"), DEBUG_STYLE);
		this.debugDataSec = this.game.add.text(0, 17*11, "DataSec: " + this.calculateDefense("external"), DEBUG_STYLE);

		this.debugGroup.add(breachDebug);
		this.debugGroup.add(this.ticksDisplay.internal);
		this.debugGroup.add(this.periodDisplay.internal);
		this.debugGroup.add(this.ticksDisplay.external);
		this.debugGroup.add(this.periodDisplay.external);
		this.debugGroup.add(this.ticksDisplay.thirdParty);
		this.debugGroup.add(this.periodDisplay.thirdParty);
		this.debugGroup.add(this.debugCash);
		this.debugGroup.add(this.debugCustomers);
		this.debugGroup.add(this.debugEmpSec);
		this.debugGroup.add(this.debugDataSec);
		if(!BREACH_DEBUG_ON) {
			this.game.world.remove(this.debugGroup);
		}
		*/
	},

	onTicked: function() {

		if(this.breachesOn && this.levelHandler.businessLevel > 0) {
			//update internal and external
			if((this.stats.emails > 0 || this.stats.credentials > 0 ||
				this.stats.creditCards > 0) && this.levelHandler.businessLevel > 0) {
				//tick the visualizer
				this.attackVisualizer.onTicked();

				//update ticks of timers
				this.ticksLeft.internal--;
				this.ticksLeft.external--;
				if(this.numPartners > 0) {
					this.ticksLeft.thirdParty--;
				}
			}
			//update thirdParty time left
			this.ticksLeft.thirdParty -= this.numPartners;

			//update displays & check for breach
			/*
			for(breachType in this.ticksLeft) {
				this.ticksDisplay[breachType].setText(breachType + " Ticks: " + this.ticksLeft[breachType]);
				if(this.ticksLeft[breachType] <= 0) {
					this.onBreach(breachType);
				}
			}

			this.debugEmpSec.setText("Emp Sec: " + this.calculateDefense("internal"));
			this.debugDataSec.setText("DataSec: " + this.calculateDefense("external"));
			*/
		}
	},

	/**
	 * project -> void
	 * updates ticksLeft & period to reflect changing threat level
	 * upon completion of new project
	 */
	updatePeriod: function(project) {
		var effects = project.effects;
		var newEmployees = effects.hasOwnProperty("numWorkers") ? effects.numWorkers : 0;
		var newEmails = effects.hasOwnProperty("emails") ? effects.emails : 0;
		var newCredentials = effects.hasOwnProperty("credentials") ? effects.credentials : 0;
		var newCreditCards = effects.hasOwnProperty("creditCards") ? effects.creditCards : 0;
		// console.log("new employees: " + newEmployees);
		var empSubtracting = newEmployees * this.periods.decrements.employees;
		// console.log("empSubtracting: " + empSubtracting);
		// console.log(this.periods);
		// console.log(project.effects);

		// console.log("Ticks Left Before: ");
		// console.log(this.ticksLeft);
		this.periods.internal.currValue -= empSubtracting;
		this.periods.internal.currValue =
			Math.max(this.periods.internal.currValue,
				this.periods.internal.minValue);
		this.ticksLeft.internal -= empSubtracting;
		// console.log("Ticks Left After: ");
		// console.log(this.ticksLeft);

		var subtracting = 0;
		subtracting += newEmails * this.periods.decrements.emails;
		// console.log("after emails: " + subtracting);
		subtracting += newCredentials * this.periods.decrements.credentials;
		// console.log("after credentials: " + subtracting);
		subtracting += newCreditCards * this.periods.decrements.creditCards;
		// console.log("after creditCards: " + subtracting);

		this.periods.external.currValue -= subtracting;
		this.periods.extenal = Math.max(this.periods.external.currValue, this.periods.external.minValue);
		this.ticksLeft.external -= subtracting;

		// this.periodDisplay.external.setText("External Pd: " + this.periods.external.currValue);
		// this.periodDisplay.internal.setText("Internal Pd: " + this.periods.internal.currValue);
	},

	onBreach: function(breachType) {
		playSound(SOUND_BREACH);

		this.timesBreached++;

		this.ticksLeft[breachType] = this.periods[breachType].currValue;

		var isThirdPary = breachType == "thirdParty";

		var defense = this.calculateDefense(breachType);

		// var strength = Math.floor(Math.random() * this.maxStrength);
		var timeElapsed = this.stats.targetDuration - this.stats.timeLeft;
		var strength = this.getStrength();
			// Math.floor((timeElapsed/ this.stats.targetDuration) * this.maxStrength);
		var damage = Math.max(0, strength - defense);
		console.log("Strength: " + strength + ". Defense: " + defense + ". Damage: " + damage);

		//TODO: sort into low, medium, high
		// var strengthCat =this.assignCategory(strength);
		// var defenseCat = this.assignCategory(defense);
		// var damageCat = this.assignDamageCategory(damage);
		// console.log("StrengthCat: " + strengthCat +
		// 	". DefenseCat: " + defenseCat +
		// 	". DamageCat: " + damageCat);


		var cost = this.dealDamage(damage, isThirdPary);

		// this.typeText.setText(breachType);
		this.setTypeText(breachType);
		// this.setStrengthText(breachType, strengthCat, defenseCat, damageCat);
		this.setStrengthText(breachType, strength, defense, damage);

		var modal = this.timesBreached <= NUM_SHOW_MODAL;
		if(modal) {
			this.showPopup();
		}
		this.showAlert(breachType, modal, cost)
		// else {
		// 	this.showAlert();
		// }
	},

	//returns either an int 0-3
	calculateDefense: function(breachType) {
		var defense = 0;
		switch(breachType) {
			case "internal":
				defense = this.stats.employeeSecurityIsOn ? 
					this.stats.employeeSecurity : 0;
				break;
			case "external":
				defense = this.stats.dataSecurityIsOn ?
					this.stats.dataSecurity : 0;
				break;
			case "thirdParty":
				//each partner has a max defense of 2 (0, 1, & 2) for (low, med, high)
				defense = (this.partnerDefense / (this.numPartners * 2)) * 3;
				break;

		}
		// console.log("Defense for " + breachType + " breach: " + defense);
		return defense;
	},

	//returns an int from 1 to 3
	getStrength: function() {
		var businessLevel = this.levelHandler.businessLevel;
		console.log("businessLevel: " + businessLevel);
		if(businessLevel < this.threshold.low) {
			return 1;
		}
		else if(businessLevel < this.threshold.high){
			return 2;
		}
		else {
			return 3;
		}
	},

	//for strength and defense, no minimal value. just HML
	// assignCategory: function(value) {
	// 	var category;
	// 	if(value >= this.threshold.high) {
	// 		category = BREACH_CATEGORIES[3];
	// 	}
	// 	else if(value >= this.threshold.low) {
	// 		category = BREACH_CATEGORIES[2];
	// 	}
	// 	else {
	// 		category = BREACH_CATEGORIES[1];
	// 	}
	// 	return category;
	// },

	assignDamageCategory: function(value) {
		var category;
		if(value >= this.damages.thresholds.high) {
			category = BREACH_CATEGORIES[3];
		}
		else if(value >= this.damages.thresholds.low) {
			category = BREACH_CATEGORIES[2];
		}
		else if(value >= this.damages.thresholds.minimal){
			category = BREACH_CATEGORIES[1];
		}
		else {
			category = BREACH_CATEGORIES[0];
		}
		return category;
	},

	setTypeText: function(breachType) {
		var string = BreachController.TYPE_TEXT_STRINGS[breachType];
		this.typeText.setText(string);
	},

	// setStrengthText: function(breachType, strengthCat, defenseCat, damageCat) {
	setStrengthText: function(breachType, strength, defense, damage) {
		var string;
		// if(breachType == "external") {
			string = [
				"Luckily, that was a low-powered attack.",
				"It was a somewhat powerful attack.",
				"That was a very sophisticated attack."][strength-1];
			string += "\nOur defenses are ";
			string += ["non-existent", "low", "moderate", "strong, thankfully"][defense];
			string += ", and we've suffered ";
			string += ["almost no", "some", "a fair amount of", "a TON of"][damage];
			string += " damage.";
		// }
		this.strengthText.setText(string);
		// this.strengthText.setText("Breach Strength (" + strength + " - " + defense + "): " + damage + ", " + damageCategory.toUpperCase());
	},

	/**
	 * Category is a value in the BREACH_CATEGORIES array.
	 * Responses is an array containing return values for each
	 * different return value.
	 * Returns w/e type is contained in responses
	 */
	switchTextOnCategory: function(category, responses){
		var toReturn;
		for(var i = 0; i < responses.length; i++) {
			if(category == BREACH_CATEGORIES[i]) {
				toReturn = responses[i];
			}
		}
		return toReturn;
	},

	showAlert: function(breachType, isModal, cost) {
		this.game.world.addChild(this.alertSprite);
		var breachPos = BREACH_ALERT_POSITIONS[breachType];
		this.alertSprite.x = breachPos.x;
		this.alertSprite.y = breachPos.y;
		var numRepeats = isModal ? -1 : 4;
		var tween = this.game.add.tween(this.alertSprite).to(
			{ y: this.alertSprite.y - 20 }, 350, "Linear", true, 0, numRepeats, true);
		if(!isModal){
			tween.onComplete.add(this.hideAlert, this);
		}
		globalPopup(this.game, this.alertSprite.x + 20, this.alertSprite.y - 20,
			"-$" + cost);
	},

	hideAlert: function(){
		this.game.world.removeChild(this.alertSprite);
	},

	onAlertClick: function() {
		this.showPopup();

		// this.alertSprite.visible = false;
		this.hideAlert();
	},

	showPopup: function() {
		this.breached.dispatch();
		this.group.visible = true;
		this.game.world.addChild(this.group);
	},

	/**
	 * string ("low", "medium", "high") -> cash lost
	 */
	dealDamage:function(damage, isThirdPary) {
		var category = BREACH_CATEGORIES[damage];
		console.log("Damage category: " + category);
		var damages = this.damages[category];
		// console.log("doing damages for category " + category);
		// console.log(damages);
		var cost = 0;

		//calculate cost and damages from data-loss
		cost += this.damageData("emails", damages, "Emails", isThirdPary);
		cost += this.damageData("credentials", damages, "Passwords", isThirdPary);
		cost += this.damageData("creditCards", damages, "Credit Cards", isThirdPary);

		var fines = damages.cash;
		cost += damages.cash;
		var fineText = fines >= 0 ? "And we've been fined $" + fines + "." : "";
		this.texts.cash.setText(fineText);

		//can't have negative customers
		var customersLost = Math.min(damages.income, this.stats.income);
		var custText = customersLost >= 0 ? "Plus " + customersLost + " of our " + this.stats.income + " customers' business." : "";
		this.texts.customers.setText(custText);

		this.stats.income -= customersLost;
		this.stats.cash -= cost;
		this.totalCashLost += cost;
		this.totalCustomersLost += customersLost;

		this.texts.totals.setText("So far, we've lost $" + this.totalCashLost.toFixed(0) + " and " +
			this.totalCustomersLost + " customers.");

		// this.debugCash.setText("Cash Lost: " + this.totalCashLost.toFixed(2));
		// this.debugCustomers.setText("Customers Lost: " + this.totalCustomersLost.toFixed(2));

		return cost;
	},

	/**
	 * string, object -> number
	 * Stat is a lookup key for what we are doing damage to.
	 * Damages is an object which contains all damages to be done for stats.
	 */
	damageData: function(statName, damages, labelString, isThirdPary) {
		var percentage = damages[statName] / 100.0;
		var statObj = isThirdPary ? this.stats.partnerData : this.stats;
		var numRecords = statObj[statName];
		if(isThirdPary) {
			labelString = "Partner " + labelString;
		}
		var recordsLost = percentage * numRecords;
		statObj[statName] -= recordsLost;

		var cost = recordsLost * this.damages.dollarsPerRecord[statName];
		this.texts[statName].setText(recordsLost.toFixed(0) + " of " + statObj[statName].toFixed(0) + " " +
			labelString + " costing $" + cost.toFixed(0));
		// this.texts[statName].setText(labelString + ": " + recordsLost.toFixed(0) + " out of " + statObj[statName].toFixed(0));

		return cost;
	},

	onContinueClick: function() {
		// this.group.destroy(true);
		this.group.visible = false;
		// this.alertSprite.visible = false;
		this.hideAlert();
		this.continued.dispatch(this);

		playSound(SOUND_CLICK);
	},

	onGameRestart: function() {
		this.group.visible = false;
		// this.alertSprite.visible = false;
		this.continued.dispatch(this);
		this.periods.internal.currValue = this.periods.internal.maxValue;
		this.periods.external.currValue = this.periods.external.maxValue;
		this.ticksLeft.internal = this.periods.internal.currValue;
		this.ticksLeft.external = this.periods.external.currValue;

		this.ticksLeft.external /= 2.0;
		//FOR DEBUGGING PURPOSES ONLY
		// this.ticksLeft.internal = 20;

		this.timesBreached = 0;
		this.totalCashLost = 0;
		this.totalCustomersLost = 0;
		this.numPartners = 0;
		this.partnerDefense = 0;

		this.attackVisualizer.onGameRestart();
	},

	onSwitchClick: function() {
		this.breachesOn = !this.breachesOn;
		var text = this.breachesOn ? "Breaches:\nON" : "Breaches:\nOFF";
		this.switchText.setText(text);
	},

	addPartner: function(newPartner) {
		console.log("added partner with security " + newPartner.securityScore);
		this.numPartners++;
		this.partnerDefense += newPartner.securityScore;
		console.log("new partner defense: " + this.partnerDefense);
	},

	removePartner: function(removedPartner) {
		console.log("removed partner with security " + removedPartner.securityScore);
		this.numPartners--;
		this.partnerDefense -= removedPartner.securityScore;
		console.log("new partner defense: " + this.partnerDefense);
	},
};