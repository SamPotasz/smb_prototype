UpgradeHandler = (function() {
	//for keeping private variables private
	var key = {};

	var UPGRADE_POPUP_X = 186;
	var UPGRADE_POPUP_Y = 4;

	var EFFECT_DISPLAY_X = 20;
	var UPGRADE_LINE_HEIGHT = 15;

	var WORKERS_NEEDED = 2;

	//offsets from the monitor's x & y
	var UPGRADE_BAR_X = 0;
	var UPGRADE_BAR_Y = 9;
	var UPGRADE_BAR_FOLDER_PREFIX = "upgradeReady_"
	var UPGRADE_BAR_FILE_PREFIX = "upgradeReady_";
	var UPGRADE_READY_ANIM_NAME = "ready";
	var UPGRADE_LABEL_FONT = {font: '14px Maven Pro'};

	//animation vars to be played when upgrade is in progress
	var UPGRADE_PROGRESS_FOLDER_PREFIX = "upgradeProgress_";
	var UPGRADE_PROGRESS_FILE_PREFIX = "upgradeProgress_";
	var UPGRADE_PROGRESS_ANIM_NAME = "upgradeProgress";

	var UPGRADE_BG_NAME = "popup_roomTwoWest";

	var UPGRADE_WORKER_NAME = "upgradeWorker_1/upgradeWorker_1_";
	var UPGRADE_WORKER_MAX_FRAME = 30;

	var UPGRADE_COST_FONT =
		{font: '18px Maven Pro', fill: 'white', stroke: 'white'};

	UpgradeHandler = function(gameParam, upgradesJson, monitorVars, workerVars) {
		var $ = {
			game : gameParam,

			upgrades : upgradesJson,
			upgradesCompleted : 0,

			workTimer : new WorkTimer(WORKERS_NEEDED, 0),
			locked : true,
			completedSignal : new Phaser.Signal(),

			startedSignal : new Phaser.Signal(),
			canceledSignal: new Phaser.Signal(),

			popupGroup : game.add.group(),
			background : undefined,
			effectDisplay : undefined,
			// timeLeftDisplay : undefined,
			workersDisplay : undefined,
			costDisplay : undefined,
			addButton : undefined,

			currWorkerTypeIndices: [],

			availX: monitorVars.x + UPGRADE_BAR_X,
			availY: monitorVars.y + UPGRADE_BAR_Y,
			barDirection: monitorVars.direction,

			workerPositions: workerVars,
			workersGroup: null,
		};
		$.hasUpgrades = !(typeof $.upgrades === "undefined");
		// console.log("workerPositions:");
		// console.log($.workerPositions);

		// Public privileged method for accessing private variables.
	    this._ = function(aKey) {
	     	if(aKey === key)
	        	return $;

	    	console.trace();
	      	throw new Error("The _() function must be called internally.");
	    };
	}

	UpgradeHandler.prototype = {
		create: function(projectPopupPosition, upgradeX, upgradeY) {
			var $ = this._(key);

			$.popupGroup.x = projectPopupPosition.x + UPGRADE_POPUP_X;
			$.popupGroup.y = projectPopupPosition.y + UPGRADE_POPUP_Y;

			$.background = $.popupGroup.create(0, 0, STILL_ATLAS_NAME, UPGRADE_BG_NAME);
			$.background.events.onInputDown.add(this.onBGDown, this);
			// $.background.inputEnabled = false;
			$.effectDisplay = $.game.add.text(0, 5, "UPGRADE", FONT);
			$.effectDisplay.lineSpacing = -5;
			// $.timeLeftDisplay = $.game.add.text(0, UPGRADE_LINE_HEIGHT * 1.5, "Work Left: ", FONT);
			$.workersDisplay = $.game.add.text(0, UPGRADE_LINE_HEIGHT * 2.5, "Workers Needed: " + $.workTimer.maxWorkers, FONT);
			$.costDisplay = $.game.add.text(0, UPGRADE_LINE_HEIGHT * 4, "Workers Needed: " + $.workTimer.maxWorkers, UPGRADE_COST_FONT);
			$.addButton = $.game.add.button(10, UPGRADE_LINE_HEIGHT * 6, STILL_ATLAS_NAME, this.onStartClicked, this,
				'buttonPopup', 'buttonPopup', 'buttonPopup', 'buttonPopup');
			labelButton($.addButton, $.game, "START UPGRADE", FONT);

			$.popupGroup.add($.effectDisplay);
			// $.popupGroup.add($.timeLeftDisplay);
			$.popupGroup.add($.workersDisplay);
			$.popupGroup.add($.costDisplay);
			$.popupGroup.add($.addButton);

			var bgWidth = $.popupGroup.width;
			$.effectDisplay.x = EFFECT_DISPLAY_X;
			// centerXWithin($.timeLeftDisplay, bgWidth);
			centerXWithin($.workersDisplay, bgWidth);
			centerXWithin($.costDisplay, bgWidth);
			centerXWithin($.addButton, bgWidth);
			$.popupGroup.visible = false;
			// console.log("popup loc: " + $.popupGroup.x + ", " + $.popupGroup.y);

			//add availiability icon
			$.availableGroup = $.game.add.group();
			$.availableGroup.x = $.availX;
			$.availableGroup.y = $.availY;

			//create bar sprite
			var availName = UPGRADE_BAR_FOLDER_PREFIX + $.barDirection.toLowerCase() + "/" +
				UPGRADE_BAR_FILE_PREFIX + $.barDirection.toLowerCase() + "_";

			$.barSprite = $.game.add.sprite(0, 0,
				ANIM_ATLAS_NAME, availName + "0");

			// labelButton($.barSprite, $.game, "UPGRADE", FONT);
			$.availableGroup.add($.barSprite);
			// $.availableGroup.add(barLabel);
			$.availableGroup.visible = false;

			//create available (looping) and progress animations
			$.barSprite.animations.add(UPGRADE_READY_ANIM_NAME,
				Phaser.Animation.generateFrameNames(availName, 0, 20),
				ANIM_FPS, true);
			var progressName = UPGRADE_PROGRESS_FOLDER_PREFIX + $.barDirection.toLowerCase() + "/" +
				UPGRADE_PROGRESS_FILE_PREFIX + $.barDirection.toLowerCase() + "_";
			$.barSprite.animations.add(UPGRADE_PROGRESS_ANIM_NAME,
				Phaser.Animation.generateFrameNames(progressName, 0, 99),
				ANIM_FPS, false);

			$.workersGroup = $.game.add.group();
			$.workersGroup.x = $.workerPositions.leftWorkerX;
			$.workersGroup.y = $.workerPositions.leftWorkerY;

			var frames = Phaser.Animation.generateFrameNames(
					UPGRADE_WORKER_NAME, 0, UPGRADE_WORKER_MAX_FRAME);

			var leftWorker = $.game.add.sprite(0, 0,
				WORKER_ANIM_ATLAS, UPGRADE_WORKER_NAME+"0");
			leftWorker.animations.add("work", frames,
				ANIM_FPS, true, false);

			if($.barDirection != "West") {
				var rightWorker = $.game.add.sprite(
					$.workerPositions.rightWorkerX - $.workersGroup.x,
					$.workerPositions.rightWorkerY - $.workersGroup.y,
					WORKER_ANIM_ATLAS, UPGRADE_WORKER_NAME + "0");
				rightWorker.x += rightWorker.width;
				rightWorker.scale.x *= -1;
				rightWorker.animations.add("work", frames,
					ANIM_FPS, true, false);
				$.workersGroup.add(rightWorker);
			}

			$.workersGroup.add(leftWorker);
			$.workersGroup.visible = false;
		},

		onBGDown: function() {
			var $ = this._(key);
			console.log("upgrade background touched");
		},

		onTicked: function() {
			var $ = this._(key);
			var timer = $.workTimer;
			if(timer.numWorkers > 0) {
				if(timer.isCompleted) {
					this.onCompleted();
				}
				else {
					timer.onTicked();
					// $.timeLeftDisplay.setText("Time Left: " + timer.timeLeft);

					var percentageComplete = (timer.maxTime - timer.timeLeft) / timer.maxTime;
					var frameNumber = Math.floor(percentageComplete * 100);
					//99 is our max frame number in the animation. So cap it.
					var frameNumber = Math.min(99, frameNumber);
					$.barSprite.animations.currentAnim.frame = frameNumber;
					// var frameTitle = TIMER_FOLDER_PREFIX + $.timerDirection.toLowerCase() + "/" +
					// 	TIMER_FILE_PREFIX + $.timerDirection.toLowerCase() + "_" + frameNumber;
					// $.timerSprite.frameName = frameTitle;
				}
			}
		},

		onCompleted: function() {
			// console.log("COMPLETED!");
			var $ = this._(key);
			var timer = $.workTimer;
			timer.resetTimer();

			var nextUpgrade = this.currUpgrade; //$.upgrades[$.upgradesCompleted];

			// console.log(nextUpgrade);

			$.upgradesCompleted++;
			$.locked = true;

			$.addButton.visible = true;
			$.popupGroup.visible = false;
			$.availableGroup.visible = false;
			$.workersGroup.visible = false;
			this.togglePopup(false);

			playSound(SOUND_UPGRADE_COMPLETE);

			$.completedSignal.dispatch(nextUpgrade.effects);
			$.canceledSignal.dispatch(this, $.currWorkerTypeIndices);
		},

		toggleLocked : function(stats){
			var $ = this._(key);
			if($.hasUpgrades && $.upgradesCompleted < $.upgrades.length) {
				var nextUpgrade = this.currUpgrade;

				//locked is whether or not all prereqs have been met
				if($.locked) {
					if(prereqsMet(nextUpgrade.prereqs, stats)) {
						var effect = nextUpgrade.effects[0];
						// $.effectDisplay.setText(
						// 	"+" + effect.value + " " + effect.key);
						$.effectDisplay.setText(nextUpgrade.flavor);

						var maxTime = nextUpgrade.duration;
						$.workTimer.maxTime = maxTime;
						$.costDisplay.setText("Cost: " + toKDollars(nextUpgrade.cost));

						$.locked = false;
						$.availableGroup.visible = true;
						$.barSprite.animations.play(UPGRADE_READY_ANIM_NAME);

						var bgWidth = $.popupGroup.width;
						$.effectDisplay.x = EFFECT_DISPLAY_X;
						centerXWithin($.workersDisplay, bgWidth);
						centerXWithin($.costDisplay, bgWidth);
						centerXWithin($.addButton, bgWidth);
					}
					else {
						$.locked = true;
						$.availableGroup.visible = false;
					}
				}
				//for unlocked upgrades, check whether we have resources to actually do it
				else if($.workTimer.numWorkers == 0) {
					var enoughCash = stats.cash >= nextUpgrade.cost || nextUpgrade.cost <= 0;
					var enoughWorkers = stats.freeWorkers >= WORKERS_NEEDED;
					if(!(enoughCash && enoughWorkers)) {
						$.effectDisplay.alpha = 0.5;
						$.workersDisplay.alpha = enoughWorkers ? 0.0 : 1.0;
						$.costDisplay.alpha = enoughCash ? 0.5 : 1.0;
						$.addButton.visible = false;
					}
					else {
						$.effectDisplay.alpha = 1.0;
						$.workersDisplay.alpha = 0.0;
						$.costDisplay.alpha = 1.0;
						$.addButton.visible = true;
					}
				}
			}
		},

		onStartClicked : function() {
			var $ = this._(key);
			console.log("starting upgrade. " + $.barSprite.animations.currentAnim.name);
			$.startedSignal.dispatch(this);
			$.barSprite.animations.currentAnim = $.barSprite.animations.getAnimation(UPGRADE_PROGRESS_ANIM_NAME);

			$.addButton.visible = false;
			$.workersGroup.visible = true;
			$.workersGroup.children[0].animations.play("work");
			$.workersGroup.children[1].animations.play("work");
		},

		onGameRestart: function(stats) {
			var $ = this._(key);
			$.upgradesCompleted = 0;
			// $.locked = true;
			this.toggleLocked(stats);
			// console.log(this.currUpgrade);
		},

		togglePopup: function(showPopup) {
			var $ = this._(key);
			var world = $.game.world;
			var popup = $.popupGroup;
			if(showPopup && !$.locked) {
				popup.visible = true;
				world.bringToTop(popup);
			}
			else {
				popup.visible = false;
			}
		},

		get workTimer() {
			return this._(key).workTimer;
		},
		get completedSignal() {
			return this._(key).completedSignal;
		},
		get startedSignal() {
			return this._(key).startedSignal;
		},
		get canceledSignal() {
			return this._(key).canceledSignal;
		},
		get currUpgrade() {
			var $ = this._(key);
			return $.upgrades[$.upgradesCompleted];
		},
		get workersNeeded() {
			return WORKERS_NEEDED;
		},
		get cost() {
			return this.currUpgrade.cost;
		},

		get hasUpgrades() { return this._(key).hasUpgrades; },

		get backgroundTouched(){
			return this._(key).background.events.onInputUp;
		},

		get upgradesCompleted(){ return this._(key).upgradesCompleted; },
	}

	return UpgradeHandler;
})();