Partner = (function(){

	//display constants
	var Y_OFFSET = 0; //135;
	var PARTNER_HEIGHT = 105;
	var PARTNER_POPUP_NAME = "popup_partnerOptionMiddle";
	var PARTNER_TEXT_X = 14;
	var PARTNER_TEXT_Y = 13;

	var PARTNER_LINE_HEIGHT = 18;

	var CLOUD_OFFSET_X = -10; //from top of room;
	var CLOUD_OFFSET_Y = 5; //from top of room;

	var MAX_ROYALTY = 18; //15;
	var MIN_ROYALTY = 2;
	var RANGE_ROYALTY = MAX_ROYALTY - MIN_ROYALTY;

	//from Naomi
	// var DURATIONS = [20, 30, 40];//[100, 200, 300];
	var SECURITIES = ["LOW", "MED", "HIGH"];
	var COSTS = [500, 1500, 4500];
	var DATA_EARNINGS = [10, 20, 30];

	var AUDIT_DURATION = 10;

	var AUDIT_BUTTON_PRE = "buttonPopupPartnerAudit/buttonPopupPartnerAudit_";
	var ACCEPT_BUTTON_PRE = "buttonPopupPartnerAccept";
	var REJECT_BUTTON_PRE = "buttonPopupPartnerReject";

	var AUDIT_BUTTON_POS = {x: 98, y: 111};
	var ACCEPT_BUTTON_POS = {x: 19, y: 111};
	var REJECT_BUTTON_POS = {x: 176, y: 111};

	var AUDIT_BUTTON_LABEL = "AUDIT";
	var ACCEPT_BUTTON_LABEL = "ACCEPT";
	var REJECT_BUTTON_LABEL = "REJECT";

	var PARTNER_BODY_FONT = {font: '12px Maven Pro', fill: 'white', stroke: 'white'};

	/** every how many ticks does this earn something? */
	var EARN_PERIOD = 10;

	var key = {};

	function Partner(gameParam, partnerStatParam, effectsParam) {
		var displayName;
		switch(partnerStatParam) {
			case "listserv":
				displayName = "POTENTIAL MAILING LIST PARTNER";
				break;
			case "forum":
				displayName = "POTENTIAL FORUM PARTNER";
				break;
			case "eCommerce":
				displayName = "POTENTIAL CHECKOUT PARTNER";
				break;
		}
		var $ = {
			name : displayName,
			partnerStat : partnerStatParam,
			cost : pickRandomFrom(COSTS),
			royalties : Math.floor(Math.random() * RANGE_ROYALTY) + MIN_ROYALTY,
			// duration : pickRandomFrom(DURATIONS),
			// timeLeft : 0,
			ticks: 0,
			security : pickRandomFrom(SECURITIES),
			isAudited : false,
			isAuditing : false,
			isActive : false,	//has been accepted
			researchStaffed: true, 	//over-arching project is staffed
			auditTimeLeft : AUDIT_DURATION,

			effects : {},

			game : gameParam,
			group : undefined,

			nameDisplay : undefined,
			costDisplay : undefined,
			royaltyDisplay: undefined,
			// durationDisplay: undefined,
			securityDisplay: undefined,
			effectsDisplay: undefined,

			auditButton: undefined,
			acceptButton: undefined,
			rejectButton: undefined,

			// reupButton: undefined,
			cancelReupButton: undefined,

			cloudButton: undefined,

			acceptClicked : new Phaser.Signal(),
			rejectClicked : new Phaser.Signal(),
			earned : new Phaser.Signal(),
			completed: new Phaser.Signal(),
			canceled: new Phaser.Signal()
		};
		// $.timeLeft = $.duration;

		var numEffects = effectsParam.length;
		for(var i = 0; i < numEffects; i++) {
			var currEffect = effectsParam[i];
			var currKey = currEffect.key;
			$.effects[currEffect.key] = pickRandomFrom(currEffect.value);
			if(currKey == "cash") {
				$.effects.cash *= (1.0 - $.royalties/100.0);
			}
		}

		this._ = function(aKey) {
			if(aKey === key)
	        	return $;

	    	console.trace();
	      	throw new Error("The _() function must be called internally.");
		}

		/*
		this.onCompleted = function() {
			console.log("partner completed.");
			$.cloudButton.alpha = 0.5;
			$.reupButton.visible = true;
			$.cancelReupButton.visible = true;
			$.completed.dispatch(this);
		},
		*/

		this.updateDisplays = function() {
			var $ = this._(key);

			$.costDisplay.setText("COST: $" + $.cost);
			$.royaltyDisplay.setText("ROYALTIES: " + $.royalties + "% of revenue");
			this.updateSecurityDisplay();
			this.updateTimeLeftDisplay();
		},

		this.updateSecurityDisplay = function() {
			var $ = this._(key);
			var secString = "SECURITY: ";
			if($.isAudited) {
				secString += $.security;
			}
			else {
				secString += "???";
			}
			$.securityDisplay.setText(secString);

			var auditVisible = !($.isAuditing || $.isAudited);
			$.auditButton.alpha = auditVisible ? 1.0 : 0.3;
		},

		this.updateTimeLeftDisplay = function() {
			var $ = this._(key);

			// $.durationDisplay.setText("DURATION: " + $.timeLeft);

			var tickedOff = AUDIT_DURATION - $.auditTimeLeft;
			var frameName = AUDIT_BUTTON_PRE + tickedOff;
			$.auditButton.setFrames(frameName, frameName, frameName, frameName);
		},

		this.onAuditClicked = function() {
			this._(key).isAuditing = true;
			playSound(SOUND_CLICK);
		},

		this.onAcceptClicked = function() {
			var $ = this._(key);
			$.acceptClicked.dispatch(this);
			$.isActive = true;

			$.group.visible = false;
			$.acceptButton.visible = false;
			$.auditButton.visible = false;
			$.rejectButton.visible = false;
			$.cancelReupButton.visible = true;

			this.updateTimeLeftDisplay();
			$.nameDisplay.setText($.partnerStat + " Partner");

			playSound(SOUND_CLICK);
		},

		this.onRejectClicked = function() {
			this._(key).rejectClicked.dispatch(this);
			playSound(SOUND_CLICK);
		},

		this.addCloudIcon = function(cloudX, cloudY) {
			var $ = this._(key);
			var cloudX = cloudX + CLOUD_OFFSET_X;
			var cloudY = cloudY + CLOUD_OFFSET_Y;
			$.cloudButton = $.game.add.button(cloudX, cloudY, STILL_ATLAS_NAME,
				this.onIconClicked, this,
				'cloud', 'cloud', 'cloud', 'cloud');
			$.group.x = $.cloudButton.x + 60;
			$.group.y = $.cloudButton.y;
		},

		this.onIconClicked = function() {
			var group = this._(key).group;
			group.visible = !group.visible;
		},

		// this.onReupClicked = function() {
		// 	var $ = this._(key);

		// 	// $.timeLeft = $.duration;
		// 	this.updateTimeLeftDisplay();

		// 	$.cloudButton.alpha = 1.0;
		// 	$.reupButton.visible = false;
		// 	$.cancelReupButton.visible = false;
		// },

		this.onCancelReupClicked = function() {
			if($.isActive){
				$.cloudButton.destroy();
				// $.reupButton.destroy();
				$.cancelReupButton.destroy();
			}
			$.isActive = false;

			$.game.world.remove($.group);
			$.group.visible = false;

			$.canceled.dispatch(this);
		}
	}

	Partner.prototype = {
		onTicked: function() {
			var $ = this._(key);
			// console.log("Ticking partner " + $.name);
			if($.isAuditing && $.researchStaffed) {
				if($.auditTimeLeft <= 0) {
					$.isAudited = true;
					$.isAuditing = false;
					this.updateSecurityDisplay();
					playSound(SOUND_CLICK);
				}
				else {
					$.auditTimeLeft--;
					this.updateTimeLeftDisplay();
				}
			}
			else if($.isActive) {
				// if($.timeLeft > 0) {
					// $.timeLeft--;
					$.ticks++;
					this.updateTimeLeftDisplay();
					if($.ticks % EARN_PERIOD == 0) {
						$.earned.dispatch($.effects);
						var popupString = "";
						for(var name in $.effects) {
							popupString += "+" + $.effects[name] + " " + name + "\n";
							globalPopup($.game, $.cloudButton.x + 10, $.cloudButton.y - 35, popupString);
						}
					}
				// }
				// else {
				// 	this.onCompleted();
				// }
			}
		},

		toString : function() {
			var string = "";
			var $ = this._(key);
			for(var name in $) {
				string += name + ": " + $[name] + ", ";
			}
			return string;
		},

		createGroup: function() { //(projectX, projectY, index) {
			var $ = this._(key);

			if(typeof $.group === "undefined") {
				$.group = $.game.add.group();

				var background = $.group.create(0, 0, STILL_ATLAS_NAME, PARTNER_POPUP_NAME);
				background.inputEnabled = true;

				$.nameDisplay = $.game.make.text(PARTNER_TEXT_X, PARTNER_TEXT_Y, $.name, FONT);
				$.nameDisplay.wordWrap = false;
				centerXWithin($.nameDisplay, background.width + 10);

				$.costDisplay = $.game.make.text(PARTNER_TEXT_X, PARTNER_TEXT_Y + PARTNER_LINE_HEIGHT, $.cost, PARTNER_BODY_FONT);
				$.royaltyDisplay = $.game.make.text(PARTNER_TEXT_X, PARTNER_TEXT_Y + 2 * PARTNER_LINE_HEIGHT, $.royalties, PARTNER_BODY_FONT);
				// $.durationDisplay = $.game.make.text(PARTNER_TEXT_X, PARTNER_TEXT_Y + 3 * PARTNER_LINE_HEIGHT, $.duration, PARTNER_BODY_FONT);
				$.securityDisplay = $.game.make.text(PARTNER_TEXT_X, PARTNER_TEXT_Y + 4 * PARTNER_LINE_HEIGHT, $.security, PARTNER_BODY_FONT);

				//set up buttons
				var auditName = AUDIT_BUTTON_PRE + "0";
				$.auditButton = $.game.make.button(AUDIT_BUTTON_POS.x, AUDIT_BUTTON_POS.y,
					ANIM_ATLAS_NAME, this.onAuditClicked, this,
					auditName, auditName, auditName,auditName);
				labelButton($.auditButton, $.game, AUDIT_BUTTON_LABEL, FONT);

				$.acceptButton = $.game.make.button(ACCEPT_BUTTON_POS.x, ACCEPT_BUTTON_POS.y,
					STILL_ATLAS_NAME, this.onAcceptClicked, this,
					ACCEPT_BUTTON_PRE, ACCEPT_BUTTON_PRE, ACCEPT_BUTTON_PRE, ACCEPT_BUTTON_PRE);
				labelButton($.acceptButton, $.game, ACCEPT_BUTTON_LABEL, FONT);

				$.rejectButton = $.game.make.button(REJECT_BUTTON_POS.x, REJECT_BUTTON_POS.y,
					STILL_ATLAS_NAME, this.onRejectClicked, this,
					REJECT_BUTTON_PRE, REJECT_BUTTON_PRE, REJECT_BUTTON_PRE,REJECT_BUTTON_PRE);
				labelButton($.rejectButton, $.game, REJECT_BUTTON_LABEL, FONT);

				// $.reupButton = $.game.make.button(PARTNER_TEXT_X, 6 * PARTNER_LINE_HEIGHT, STILL_ATLAS_NAME, this.onReupClicked, this,
				// 	'buttonPopupGreen', 'buttonPopupGreen', 'buttonPopupGreen', 'buttonPopupGreen');
				// labelButton($.reupButton, $.game, "RE-UP PARTNER", FONT);
				// $.reupButton.visible = false;

				$.cancelReupButton = $.game.make.button(80, 8 * PARTNER_LINE_HEIGHT,
					STILL_ATLAS_NAME, this.onCancelReupClicked, this,
					'buttonPopupRed', 'buttonPopupRed', 'buttonPopupRed', 'buttonPopupRed');
				$.cancelReupButton.visible = false;
				labelButton($.cancelReupButton, $.game, "CANCEL", FONT);

				$.group.add(background);
				$.group.add($.nameDisplay);
				$.group.add($.costDisplay);
				$.group.add($.royaltyDisplay);
				$.group.add($.securityDisplay);
				// $.group.add($.durationDisplay);
				$.group.add($.auditButton);
				$.group.add($.acceptButton);
				$.group.add($.rejectButton);
				// $.group.add($.reupButton);
				$.group.add($.cancelReupButton);

				$.group.visible = false;
				this.updateDisplays();

				// $.optionButton = $.game.add.button(0, 0, STILL_ATLAS_NAME,
				// 	this.onOptionClicked, this,
				// 	'buttonPopup', 'buttonPopup', 'buttonPopup', 'buttonPopup');
			}
		},

		toggleLocked: function(locked) {
			var $ = this._(key);
			console.log("toggling locked for partner. locked? " + locked);
			console.log($.cloudButton);
			if(!isUndefined($.cloudButton)) {
				$.cloudButton.visible = !locked;
			}
		},

		onGameRestart: function() {
			// console.log("restarting " + this._(key).name);
			this.onCancelReupClicked();
		},

		get group() {
			return this._(key).group;
		},

		get acceptSignal() {
			return this._(key).acceptClicked;
		},
		get rejectClicked(){ return this._(key).rejectClicked; },

		get partnerStat() { return this._(key).partnerStat; },

		/** signal that our period is up. time to collect data / money! */
		get earned() { return this._(key).earned; },

		/** how much this partner is earning per tick */
		get earning() {
			return this._(key).effects.cash / EARN_PERIOD;
		},

		get canceledSignal(){ return this._(key).canceled; },

		get isActive(){ return this._(key).isActive; },

		/**
		 * returns 0, 1, 2
		 */
		get securityScore(){
			return SECURITIES.indexOf(this._(key).security);
		},

		get completed(){ return this._(key).completed; },

		// get optionButton(){ return this._(key).optionButton; },
		// get optionClicked(){ return this._(key).optionClicked; },
	};

	return Partner;
})();
