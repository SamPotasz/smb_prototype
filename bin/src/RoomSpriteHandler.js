var INFO_IN_CORNER = false;

var PADDING_X = 3;
var PADDING_Y = 3;

var POPUP_Y_OFFSET = -115;
var POPUP_X_OFFSET = 20;

var MORE_INFO_WIDTH = INFO_IN_CORNER ? 330 : 210;
var MORE_INFO_BOTTOM_BORDER = 10;
var MORE_INFO_TEXT_PADDING = 3;
var MORE_INFO_STYLE = INFO_IN_CORNER ? TUTORIAL_STYLE :
	{font: '12px Open Sans', wordWrap: true, wordWrapWidth: MORE_INFO_WIDTH};

var LOCKED_SPRITE_PREFIX = "locked_";
var CLICK_SPRITE_PREFIX = "roomTarget_";

RoomSpriteHandler = (function(){

	var key = {};

	function RoomSpriteHandler(gameParam, artJson, idParam, moreInfoStringParam) {
		var $ = {
			game : gameParam,
			bigGroup : game.add.group(),
			popupGroup : game.add.group(),
			background: undefined,

			moreInfoString: moreInfoStringParam,
			moreInfoGroup: undefined,
			moreInfoText: undefined,

			artVars : artJson,
			name : idParam,

			x : artJson.clickX,
			y : artJson.clickY,

			backgroundTouched : new Phaser.Signal(),
		};

		this._ = function(aKey) {
			if(aKey === key)
	        	return $;

	    	console.trace();
	      	throw new Error("The _() function must be called internally.");
		}

		/**
		 * @param popupBelow determines whether or not we
		 * put the popupGroup below the BG sprite (instead of above)
		 */
		this.create = function() {

			var lockedName = LOCKED_SPRITE_PREFIX + $.name;

			//some projects don't have a locked sprite (and won't be used)
			if($.name == "hire" || $.name.indexOf("Partners") >= 0) {
				lockedName = "locked_fire";
			}

			$.lockedSprite = $.game.add.sprite($.artVars.lockedX, $.artVars.lockedY,
				STILL_ATLAS_NAME, lockedName);
			$.lockedSprite.visible = false;

			var bgName = CLICK_SPRITE_PREFIX + $.name;
			$.background = $.game.add.sprite(0, 0, STILL_ATLAS_NAME, bgName);
			$.background.inputEnabled = true;
			$.background.input.pixelPerfectClick = true;
			$.background.input.pixelPerfectAlpha = 0.1;
			$.background.events.onInputUp.add(this.onBackgroundTouch, this);


			$.background.x = $.x;
			$.background.y = $.y;
			$.popupGroup.x = $.artVars.popup.x > 0 ?
				$.artVars.popup.x : $.x + POPUP_X_OFFSET;
			$.popupGroup.y = $.artVars.popup.y > 0 ?
				$.artVars.popup.y : Math.max($.y + POPUP_Y_OFFSET, 2);

			var popupBG = $.popupGroup.create(0, 0, STILL_ATLAS_NAME, $.artVars.popup.spriteName);
			popupBG.inputEnabled = true;
			$.popupGroup.visible = false;
			$.popupBackgroundWidth = popupBG.width;
			$.popupBackgroundHeight = popupBG.height;
			popupBG.events.onInputUp.add(this.onBackgroundTouch, this);

			//create more info popup
			$.moreInfoGroup = $.game.add.group();

			$.moreInfoText = $.game.add.text(MORE_INFO_TEXT_PADDING, MORE_INFO_TEXT_PADDING,
				$.moreInfoString, MORE_INFO_STYLE);

			var bmpWidth = MORE_INFO_WIDTH + 2 * MORE_INFO_TEXT_PADDING;
			var bmpHeight = $.moreInfoText.height + 2 * MORE_INFO_TEXT_PADDING;
			var bitmap = $.game.add.bitmapData(bmpWidth, bmpHeight);
		    bitmap.ctx.beginPath();
		    bitmap.ctx.rect(0,0,bmpWidth,bmpHeight);
		    bitmap.ctx.fillStyle = '#FFFFFF';
		    bitmap.ctx.fill();
			var background = $.game.add.sprite(0, 0, bitmap);

			if(INFO_IN_CORNER) {
				$.moreInfoGroup.x = TUT_BG_X;
				$.moreInfoGroup.y = GAME_HEIGHT - background.height - MORE_INFO_BOTTOM_BORDER;
			}
			else {
				$.moreInfoGroup.x = Math.min( Math.max( $.popupGroup.x - 30, 4 ), GAME_WIDTH - bmpWidth - 4);
				$.moreInfoGroup.y = Math.min($.popupGroup.y + 210, GAME_HEIGHT - bmpHeight - 4);
			}
			$.moreInfoGroup.add(background);
			$.moreInfoGroup.add($.moreInfoText);

			$.game.world.removeChild($.popupGroup);
			$.game.world.removeChild($.moreInfoGroup);
		}
	};

	RoomSpriteHandler.prototype = {

		showLocked: function(locked) {
			var $ = this._(key);
			if(!isUndefined($.lockedSprite)){
				$.lockedSprite.visible = locked;
			}
		},

		onBackgroundTouch: function() {
			var $ = this._(key);

			if($.game.world.getIndex($.popupGroup) != -1) {
				this.hidePopup();
				$.backgroundTouched.dispatch(false);
			}
			else {
				$.game.world.addChild($.popupGroup);
				$.popupGroup.visible = true;

				$.backgroundTouched.dispatch(true);
			}
		},

		hidePopup: function() {
			var $ = this._(key);
			$.game.world.removeChild($.popupGroup);
			$.popupGroup.visible = false;

			$.game.world.removeChild($.moreInfoGroup);
			$.moreInfoGroup.visible = false;
		},

		addGroup: function(toAdd) {
			var $ = this._(key);
			// toAdd.x += PADDING_X;
			toAdd.y += PADDING_Y;
			$.popupGroup.add(toAdd);
		},

		onGameRestart: function() {
			var $ = this._(key);
			// var world = $.game.world;
			// var popup = $.popupGroup;

			// world.removeChild(popup);
			// popup.visible = false;
			this.hidePopup();
			this.inputEnabled = true;
		},

		get x(){ return this._(key).x; },
		get y(){ return this._(key).y; },

		get popupGroup(){ return this._(key).popupGroup; },
		get popupPosition() {
			var popup = this._(key).popupGroup;
			return {x: popup.x, y: popup.y};
		},

		get addButton(){ return this._(key).addButton; },
		get minusButton(){ return this._(key).minusButton; },

		get backgroundTouched(){ return this._(key).backgroundTouched; },


		/**
		 * Used to enable / disable input based on tutorial
		 */
		set inputEnabled(value) {
			this._(key).background.inputEnabled = value;
		},

		get popupBackgroundWidth(){ return this._(key).popupBackgroundWidth; },
		get popupBackgroundHeight(){ return this._(key).popupBackgroundHeight; },
	};

	return RoomSpriteHandler;
})();
