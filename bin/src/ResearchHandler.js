ResearchHandler = (function(){

	var key = {};

	var PARTNER_X_OFFSET = -269; //-186;
	var PARTNER_Y_OFFSET = -41; //1;
	var PARTNER_HEIGHT = 161;

	var RH_MONITOR_PREFIX = "partners_";
	var RH_BUTTON_X_OFFSET = 8;
	var RH_BUTTON_Y_OFFSET = 33;

	var RH_TITLE_X = 8;
	var RH_TITLE_Y = 7;

	//x will be centered
	var PARTNER_OPTIONS_Y = 122;
	var PARTNER_OPTIONS_LINE_HEIGHT = 20;
	var PARTNER_OPTION_BUTTON_NAME = "buttonPopup";

	var RESEARCH_BUTTON_NAME = "buttonPartnersScreen/buttonPartnersScreen_"; //"partners_button";
	var RESULTS_BUTTON_NAME = "buttonPartnerResults";

	var RESEARCH_FONT = {font: '10px Maven Pro', fill: 'white', stroke: 'white'};

	function ResearchHandler(gameParam, json){
		var $ = {
			game : gameParam,
			key: json.key,
			partnerStat : json.partnerStat,
			effects : json.levels[0].effects,
			artVars: json.artVars,

			locked: true,

			possiblePartners : [],
			activePartner: null,
			// hasActivePartner : false,

			researchTimer: new WorkTimer(1, json.levels[0].duration),

			monitor: null,
			titleDisplay: null,
			researchButton: null,
			resultsButton: null,

			resultsGroup : null,
			resultsVisible : false,
			selectedOption: null,

			researchCompleted: new Phaser.Signal(),
			partnerAccepted : new Phaser.Signal(),
			partnerCompleted: new Phaser.Signal(),
			partnerCanceled: new Phaser.Signal(),

			uiFocused: new Phaser.Signal(), //dispatched when anything is clicked
		};

		this._ = function(aKey) {
			if(aKey === key)
	        	return $;

	    	console.trace();
	      	throw new Error("The _() function must be called internally.");
		}
	}

	ResearchHandler.prototype = {

		create: function() {
			var $ = this._(key);

			// console.log("Creating researcher for " + $.partnerStat);
			$.monitor = $.game.add.sprite($.artVars.monitorX, $.artVars.monitorY,
				STILL_ATLAS_NAME, RH_MONITOR_PREFIX + $.key);

			var titleString;
			switch($.partnerStat) {
				case "listserv":
					titleString = "MAILING LIST";
					break;
				case "forum":
					titleString = "FORUM";
					break;
				case "eCommerce":
					titleString = "CHECKOUT";
					break;
			}
			$.titleDisplay = $.game.add.text(RH_TITLE_X, RH_TITLE_Y,
				titleString + "\nPARTNERS", RESEARCH_FONT);
			$.titleDisplay.lineSpacing = -4;
			$.monitor.addChild($.titleDisplay);
			$.monitor.inputEnabled = true;
			$.monitor.events.onInputUp.add(this.onMonitorClicked, this);

			var buttonX = $.artVars.monitorX + RH_BUTTON_X_OFFSET;
			var buttonY = $.artVars.monitorY + RH_BUTTON_Y_OFFSET;

			$.researchButton = $.game.add.button(buttonX, buttonY,
				ANIM_ATLAS_NAME, this.onResearchClicked, this,
					RESEARCH_BUTTON_NAME + "0", RESEARCH_BUTTON_NAME + "0", RESEARCH_BUTTON_NAME + "0", RESEARCH_BUTTON_NAME + "0");
			labelButton($.researchButton, $.game, "RESEARCH", RESEARCH_FONT);

			$.resultsButton = $.game.add.button(buttonX, buttonY,
				STILL_ATLAS_NAME, this.onResultsClicked, this,
					RESULTS_BUTTON_NAME, RESULTS_BUTTON_NAME, RESULTS_BUTTON_NAME, RESULTS_BUTTON_NAME);
			labelButton($.resultsButton, $.game, "RESULTS", RESEARCH_FONT);

			$.resultsGroup = $.game.add.group();
			$.resultsGroup.x = $.artVars.monitorX + PARTNER_X_OFFSET; //$.artVars.popup.x;
			$.resultsGroup.y = $.artVars.monitorY + PARTNER_Y_OFFSET; //$.artVars.popup.y;

			this.updateButtons();
		},

		onTicked: function() {
			var $ = this._(key);
			var partners = $.possiblePartners;
			for(var i = 0; i < partners.length; i++) {
				partners[i].onTicked();
			}

			$.researchTimer.onTicked();
			// if($.researchTimer.numWorkers > 0) {
				var tickedOff = $.researchTimer.maxTime - $.researchTimer.timeLeft;
				var frameName = RESEARCH_BUTTON_NAME + tickedOff;
				$.researchButton.setFrames(frameName, frameName, frameName, frameName);
			// }
			// console.log($.partnerStat + " workers: " + $.researchTimer.numWorkers + ". research left: " + $.researchTimer.timeLeft);
			if($.researchTimer.isCompleted) {
				this.onResearchComplete();
			}
		},

		onResearchClicked: function() {
			var $ = this._(key);
			console.log("Clicked " + $.key);
			$.researchTimer.numWorkers = 1;
			$.uiFocused.dispatch(this);
		},

		/**
		 * When the project is complete, make a new partner
		 * and add it to the list of researched partners
		 */
		onResearchComplete : function() {
			var $ = this._(key);
			$.researchTimer.numWorkers = 0;
			$.researchTimer.resetTimer();

			var newPartner = new Partner($.game, $.partnerStat, $.effects);
			$.selectedOption = newPartner;
			$.possiblePartners.push(newPartner);

			newPartner.createGroup();
			newPartner.group.x = $.resultsGroup.x;// + PARTNER_X_OFFSET;
			newPartner.group.y = $.resultsGroup.y;// + PARTNER_Y_OFFSET;
			newPartner.group.visible = false;

			newPartner.acceptSignal.add(this.onAcceptClicked, this);
			newPartner.canceledSignal.add(this.onPartnerCanceled, this);
			newPartner.rejectClicked.add(this.onRejectClicked, this);

			$.resultsVisible = true;
			this.updateDisplays();

			$.uiFocused.dispatch(this);
		},

		onMonitorClicked: function() {
			var $ = this._(key);
			if(this.hasActivePartner) {
				$.activePartner.onIconClicked();
			}
			else {
				this.onResultsClicked();
			}
		},

		onResultsClicked: function() {
			var $ = this._(key);
			$.resultsVisible = !$.resultsVisible;
			this.updateDisplays();
			$.uiFocused.dispatch(this);
		},

		onProjectBackgroundTouched: function(showResults) {

			var $ = this._(key);
			$.resultsVisible = showResults;
			this.updateDisplays();
		},

		updateDisplays: function() {
			var $ = this._(key);

			var partners = $.possiblePartners;
			var numPartners = partners.length;
			for(var i = 0; i < numPartners; i++) {
				var currPartner = partners[i];
				currPartner.group.visible = $.resultsVisible;
				currPartner.group.y = $.artVars.popup.y + i * PARTNER_HEIGHT;
			}

			this.updateButtons();
		},

		updateButtons: function() {
			var $ = this._(key);
			var numPartners = $.possiblePartners.length
			var researchedPartners = numPartners > 0;

			$.resultsButton.visible = researchedPartners &&
				!this.hasActivePartner && !$.locked;

			if(researchedPartners) {
				$.researchButton.x = $.artVars.popup.x;
				$.researchButton.y = $.artVars.popup.y + numPartners * PARTNER_HEIGHT;
			}
			else {
				$.researchButton.x = $.artVars.monitorX + RH_BUTTON_X_OFFSET;
				$.researchButton.y = $.artVars.monitorY + RH_BUTTON_Y_OFFSET;
			}

			$.researchButton.visible = !this.hasActivePartner && !$.locked &&
				((researchedPartners && $.resultsVisible) || !researchedPartners);
		},

		/**
		 * When a possible partner's ACCEPT button is clicked
		 */
		onAcceptClicked: function(partner) {
			var $ = this._(key);
			$.partnerAccepted.dispatch(partner);
			$.activePartner = partner;
			while($.possiblePartners.length > 0) {
				var popped = $.possiblePartners.pop();
				popped.group.visible = false;
			}

			//add cloud icon
			partner.addCloudIcon($.artVars.monitorX + RH_BUTTON_X_OFFSET,
				$.artVars.monitorY + RH_BUTTON_Y_OFFSET);

			this.updateDisplays();
		},

		onRejectClicked: function(partner) {
			partner.group.visible = false;
			var $ = this._(key);
			var index = $.possiblePartners.indexOf(partner);
			$.possiblePartners.splice(index, 1);
			this.updateDisplays();
		},

		/**
		 * when a partner runs out of time & is not re-upped
		 */
		onPartnerCanceled: function(partner) {
			var $ = this._(key);
			$.activePartner = null,
			$.researchButton.visible = true;
			$.partnerCanceled.dispatch(partner);
		},

		onPartnerFinished: function(partner) {
			this._(key).partnerCompleted.dispatch(partner);
		},

		onGameRestart: function() {
			// console.log("restarting research handler");
			var $ = this._(key);
			while($.possiblePartners.length > 0) {
				var currPartner = $.possiblePartners.pop();
				currPartner.onGameRestart();
			}
			this.updateDisplays();
		},

		toggleLocked: function(locked) {
			var $ = this._(key);
			$.locked = locked;
			var unlocked = !locked;

			$.monitor.visible = unlocked;
			$.titleDisplay.visible = unlocked;
			$.researchButton.visible = unlocked;
			$.resultsButton.visible = unlocked;

			// var alpha = unlocked ? 1.0 : 0.4;
			// $.titleDisplay.alpha = alpha;
			// $.researchButton.alpha = alpha;
			// $.resultsButton.alpha = alpha;

			$.researchButton.inputEnabled = unlocked;
			$.resultsButton.inputEnabled = unlocked;

			if($.resultsVisible) {
				$.resultsVisible = unlocked;
			}
			this.updateDisplays();
		},

		get researchCompleted(){ return this._(key).researchCompleted; },
		get partnerAcceptedSignal() {
			return this._(key).partnerAccepted;
		},
		get partnerCompletedSignal() {
			return this._(key).partnerCompleted;
		},
		get hasActivePartner() { return this._(key).activePartner != null; },

		get partnerCanceled(){ return this._(key).partnerCanceled; },

		get partnerStat(){ return this._(key).partnerStat; },
		get uiFocused(){ return this._(key).uiFocused; },
	};

	return ResearchHandler;
})();
