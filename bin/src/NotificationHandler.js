NotificationHandler = (function(){

	var key = {};

	function NotificationHandler(_game, _json) {
		var $ = {
			game: _game,
			notifications: _json,

			group: undefined,
			textBox: undefined,

			paused: new Phaser.Signal(),
			continued: new Phaser.Signal(),
		};

		var numNotes = $.notifications.length;
		for(var i = 0; i < numNotes; i++) {
			$.notifications[i].used = false;
		}

		this._ = function(aKey) {
			if(aKey === key)
	        	return $;

	    	console.trace();
	      	throw new Error("The _() function must be called internally.");
		}
	}

	NotificationHandler.prototype = {
		create: function() {
			var $ = this._(key);
			$.group = $.game.add.group();
			$.group.x = TUT_BG_X;
			$.group.y = TUT_BG_Y;

			var background = $.game.add.sprite(0, 0, ATLAS_NAME, TUT_BG_NAME);
			$.textBox = $.game.add.text(10, 10, "", TUTORIAL_STYLE);

			var button = $.game.add.button(0, NEXT_Y, ATLAS_NAME,
				this.onButtonClicked, this,
				"buttonPopup", "buttonPopup", "buttonPopup", "buttonPopup");
			labelButton(button, $.game, "CONTINUE", TUTORIAL_STYLE);
			centerXWithin(button, background.width);

			$.group.add(background);
			$.group.add($.textBox);
			$.group.add(button);

			$.game.world.removeChild($.group);
		},

		onTicked: function(stats) {
			var $ = this._(key);
			var numNotes = $.notifications.length;
			for(var i = 0; i < numNotes; i++) {
				var currNote = $.notifications[i];
				if(!currNote.used){
					var prereqs = currNote.toMoveOn;
					var conditionsMet;
					switch(prereqs.operator) {
						case "equals":
							conditionsMet = stats[prereqs.key] == prereqs.value;
							break;
						case "lessThan":
							conditionsMet = stats[prereqs.key] < prereqs.value;
					}

					if(conditionsMet) {
						currNote.used = true;
						this.displayNotification(currNote);
						$.paused.dispatch();
					}
				}
			}
		},

		displayNotification: function(notification) {
			var $ = this._(key);
			$.textBox.setText(notification.text);
			$.game.world.addChild($.group);
		},

		onButtonClicked: function() {
			var $ = this._(key);
			$.game.world.removeChild($.group);
			$.continued.dispatch();
		},

		get paused(){ return this._(key).paused; },
		get continued(){ return this._(key).continued; },
	};

	return NotificationHandler;
})();