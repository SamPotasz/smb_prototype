BusinessLevelHandler = (function(){

	var key = {};

	var BL_MONITOR_BG = "HUD_businessLevel";
	var BL_BUTTON_GRAY = "HUD_businessLevel_buttonUpgradeGray";
	var BL_BUTTON_GREEN = "HUD_businessLevel_buttonUpgrade";

	var BL_MONITOR_POS = {x: 422, y: 512}; //{x: 537, y: 512};
	var BL_TITLE_POS = {x: 170, y: 18};
	var BL_NUMBER_POS = {x: 177, y: 31};
	var BL_BUTTON_POS = {x: 35, y: 50};

	var BL_TITLE_TEXT = "LEVEL";
	var BL_NUMBER_FONT = {font: '20px Maven Pro', fill: 'white', stroke: 'white'};
	var BL_GRAY_FONT = {font: '14px Maven Pro', fill: 'gray', stroke: 'gray'};

	function BusinessLevelHandler(gameParam, levelsParam) {
		var $ = {
			game: gameParam,
			cashLevels: levelsParam,
			currLevel: 0,

			numberText: null,
			upgradeButton: null,
			upgradeText: null,

			upgraded: new Phaser.Signal(),
		};

		this._ = function(aKey) {
			if(aKey === key)
	        	return $;

	    	console.trace();
	      	throw new Error("The _() function must be called internally.");
		}
	}

	BusinessLevelHandler.prototype = {
		create: function(){
			var $ = this._(key);
			var monitor = $.game.add.sprite(BL_MONITOR_POS.x, BL_MONITOR_POS.y,
				STILL_ATLAS_NAME, BL_MONITOR_BG);
			var title = $.game.add.text(BL_TITLE_POS.x, BL_TITLE_POS.y,
				BL_TITLE_TEXT, FONT);

			var monitorWidth = monitor.width;
			$.numberText = $.game.add.text(BL_NUMBER_POS.x, BL_NUMBER_POS.y,
				"1", BL_NUMBER_FONT);

			var fakeButton = $.game.add.sprite(BL_BUTTON_POS.x, BL_BUTTON_POS.y,
				STILL_ATLAS_NAME, BL_BUTTON_GRAY);
			$.upgradeText = $.game.add.text(0, BL_BUTTON_POS.y + 2, "UPGRADE AT ", BL_GRAY_FONT);

			$.upgradeButton = $.game.add.button(BL_BUTTON_POS.x, BL_BUTTON_POS.y,
				STILL_ATLAS_NAME, this.onUpgradeTouched, this,
				BL_BUTTON_GREEN, BL_BUTTON_GREEN, BL_BUTTON_GREEN, BL_BUTTON_GREEN);
			var label = labelButton($.upgradeButton, $.game, "UPGRADE", FONT);
			label.y += 2;

			monitor.addChild(title);
			monitor.addChild($.numberText);
			monitor.addChild(fakeButton);
			monitor.addChild($.upgradeText);
			monitor.addChild($.upgradeButton);

			this.updateButtons(smb.constants.earningConstants.startingCash);
		},

		onTicked: function(cash) {
			this.updateButtons(cash);
		},

		onUpgradeTouched: function() {
			var $ = this._(key);
			$.currLevel++;
			this.updateButtons();

			$.upgraded.dispatch();
		},

		onGameRestart: function() {
			currLevel = 0;
			this.updateButtons(smb.constants.earningConstants.startingCash);
		},

		updateButtons: function(cash) {
			var $ = this._(key);
			$.numberText.setText($.currLevel + 1);
			if($.currLevel < $.cashLevels.length) {
				var cashNeeded = $.cashLevels[$.currLevel];
				$.upgradeButton.visible = cash >= cashNeeded;

				$.upgradeText.setText("UPGRADE AT " + toKDollars(cashNeeded));
				centerTextWithin($.upgradeText, $.upgradeButton);
			}
			else {
				$.upgradeButton.visible = false;
				$.upgradeText.setText("WIN AT " + toKDollars(smb.constants.earningConstants.targetCash));
			}
		},

		get upgraded(){ return this._(key).upgraded; },

		get businessLevel(){
			return this._(key).currLevel;
		}
	};

	return BusinessLevelHandler;
})();














