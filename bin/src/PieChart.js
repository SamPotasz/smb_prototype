PieChart = (function(){

	var key = {};

	/**
	 * Pie Charts display a percentage complete from 0-100.
	 * Frames go from 0-100, too
	 */
	function PieChart(gameP, xP, yP, assetPrefixP) {
		var $ = {
			game: gameP,
			x: xP,
			y: yP,
			assetPrefix: assetPrefixP,

			sprite: null,
		};

		this._ = function(aKey) {
			if(aKey === key)
	        	return $;

	    	console.trace();
	      	throw new Error("The _() function must be called internally.");
		}
	}

	PieChart.prototype = {
		create: function() {
			var $ = this._(key);
			$.sprite = $.game.add.sprite($.x, $.y, ANIM_ATLAS_NAME,
				$.assetPrefix + Math.round(Math.random()*100));
		},

		/**
		 * Sets frame according to @percent.
		 * @percent is from 0-1.
		 */
		displayPercentage: function(percent) {
			var $ = this._(key);
			var frameNumber = (percent * 100).toFixed(0);
			$.sprite.frameName = $.assetPrefix + frameNumber;
		},
	};

	return PieChart;
})();
