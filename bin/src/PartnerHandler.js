PartnerHandler = (function(){

	var key = {};

	var PARTNER_FLAVOR_X = 12;
	var PARTNER_FLAVOR_Y = 2;

	function PartnerHandler(gameP, partnerJson, statsP) {
		var $ = {
			game : gameP,
			stats: statsP,

			moreInfo: partnerJson.staffing.moreInfo,

			//staffing is a continuous project?
			isStaffed: false,
			roomSpriteHandler : new RoomSpriteHandler(game, partnerJson.staffing, "ccPartners", ""),
			workersDisplay : new WorkersDisplay(game, partnerJson.staffing.deskVars, "ccPartners"),

			researchers: [],
			partners : [],

			earned : new Phaser.Signal(),
			partnerAccepted: new Phaser.Signal(),
			partnerCanceled: new Phaser.Signal(),
			staffedChanged: new Phaser.Signal(),
		};

		var numResearchers = partnerJson.researchers.length;
		for(var i = 0; i < numResearchers; i++) {
			// console.log("i: " + i);
			var researchKey = partnerJson.researchers[i].partnerStat;
			$.researchers[researchKey] = new ResearchHandler($.game, partnerJson.researchers[i]);
			// console.log($.researchers[i]);
		}

		this._ = function(aKey) {
			if(aKey === key)
	        	return $;

	    	console.trace();
	      	throw new Error("The _() function must be called internally.");
		}
	}

	PartnerHandler.prototype = {
		create: function() {
			var $ = this._(key);
			$.roomSpriteHandler.create();
			$.workersDisplay.create(
				{x: $.roomSpriteHandler.x, y: $.roomSpriteHandler.y},
				1);

			var flavorText = $.game.add.text(PARTNER_FLAVOR_X, PARTNER_FLAVOR_Y,
				$.moreInfo, FONT);

			var employeeOptionsY = $.roomSpriteHandler.popupBackgroundHeight - 58;
			$.addButton = $.game.add.button(ADD_BUTTON_X, employeeOptionsY,
				STILL_ATLAS_NAME, this.addWorkerListener, this,
				ADD_BUTTON_NAME, ADD_BUTTON_NAME, ADD_BUTTON_NAME, ADD_BUTTON_NAME);

			var minusY = $.addButton.y;
			$.minusButton = $.game.add.button(MINUS_BUTTON_X, employeeOptionsY,
				STILL_ATLAS_NAME, this.removeWorkerListener, this,
				MINUS_BUTTON_NAME, MINUS_BUTTON_NAME, MINUS_BUTTON_NAME, MINUS_BUTTON_NAME);

			$.workerIcon = $.game.add.sprite(POPUP_WORKER_X, employeeOptionsY,
				STILL_ATLAS_NAME, POPUP_WORKER_NAME);

			var buttonGroup = $.game.add.group();
			buttonGroup.add(flavorText);
			buttonGroup.add($.addButton);
			buttonGroup.add($.minusButton);
			buttonGroup.add($.workerIcon);
			$.roomSpriteHandler.addGroup(buttonGroup);

			this.createResearchers();

			this.updateDisplay();
			this.setResearchLocked();
		},

		createResearchers: function(){
			var $ = this._(key);
			for(var researchKey in $.researchers) {
				var researcher = $.researchers[researchKey];
				researcher.create();
				researcher.partnerAcceptedSignal.add(this.onPartnerAccepted, this);
				researcher.partnerCompletedSignal.add(this.onPartnerAccepted, this);
				researcher.partnerCanceled.add(this.onPartnerCanceled, this);
				researcher.uiFocused.add(this.researchUIFocused, this);
			}
		},


		/**
		 * We need to show the popup and hide all others
		 */
		researchUIFocused: function(researchHandler) {
			this.hidePopups();
			this._(key).researchers[researchHandler.partnerStat].onProjectBackgroundTouched(true);

			playSound(SOUND_CLICK);
		},

		addWorkerListener: function() {
			var $ = this._(key);
			if($.stats.freeWorkers > 0) {
				$.stats.onWorkerAdded();
				$.isStaffed = true;
				this.updateDisplay();
				this.setResearchLocked();

				playSound(SOUND_WORKER_ADDED);
			}
		},

		removeWorkerListener: function() {
			var $ = this._(key);
			$.stats.onWorkerRemoved();
			$.isStaffed = false;
			this.updateDisplay();
			this.setResearchLocked();

			playSound(SOUND_WORKER_REMOVED);
		},

		updateDisplay: function(){
			var $ = this._(key);
			$.addButton.inputEnabled = !$.isStaffed && $.stats.freeWorkers > 0;
			$.minusButton.inputEnabled = $.isStaffed;

			$.addButton.alpha = $.addButton.inputEnabled ? 1.0 : 0.4;
			$.minusButton.alpha = $.minusButton.inputEnabled ? 1.0 : 0.4;

			var numWorkers = $.isStaffed ? 1 : 0;
			$.workersDisplay.updateDisplay(numWorkers, 1);
		},

		setResearchLocked: function() {
			var $ = this._(key);
			var locked = !$.isStaffed;

			for(var researchKey in $.researchers) {
				var researcher = $.researchers[researchKey];
				researcher.toggleLocked(locked);
			}

			for(var i = 0; i < $.partners.length; i++) {
				$.partners[i].toggleLocked(locked);
			}
		},

		onTicked: function() {
			var $ = this._(key);
			if($.isStaffed) {
				var numPartners = $.partners.length;
				var numActivePartners = 0;
				for(var i = 0; i < numPartners; i++) {
					$.partners[i].onTicked();
					if($.partners[i].isActive){
						numActivePartners++;
					}
				}

				for(var researchKey in $.researchers) {
					$.researchers[researchKey].onTicked();
				}

				this.updateDisplay();
			}
		},

		onPartnerEarned: function(effects) {
			this._(key).earned.dispatch(effects);
		},

		onGameRestart: function() {
			// console.log("restarting partner handler");
			var $ = this._(key);
			while($.partners.length > 0) {
				var currPartner = $.partners.pop();
				// console.log("found a partner. active? " + currPartner.isActive);
				if(currPartner.isActive) {
					// console.log("restarting partner");
					currPartner.onGameRestart();
				}
			}

			for(var researchKey in $.researchers) {
				$.researchers[researchKey].onGameRestart();
			}

			$.isStaffed = false;
		},

		hidePopups: function() {
			var $ = this._(key);
			$.roomSpriteHandler.hidePopup();

			for(var researchKey in $.researchers) {
				$.researchers[researchKey].onProjectBackgroundTouched(false);
			}

			playSound(SOUND_CLICK);
		},

		/**
		 * A research project has had one of its researched projects activated
		 */
		onPartnerAccepted: function(partner) {
			// console.log("Partner accepted in project");
			var $ = this._(key);
			$.partners.push(partner);
			partner.earned.add(this.onPartnerEarned, this);
			// partner.completed.add(this.onPartnerCompleted, this);
			$.roomSpriteHandler.hidePopup();

			$.partnerAccepted.dispatch(partner);

			playSound(SOUND_CLICK);
		},

		onPartnerCanceled: function(partner) {
			this._(key).partnerCanceled.dispatch(partner);

			playSound(SOUND_CLICK);
		},

		// onPartnerCompleted: function(partner) {
		// 	var $ = this._(key);
		// 	console.log("Partner completed. staffed? " + $.isStaffed);

		// 	if($.isStaffed) {
		// 		console.log("is staffed")
		// 		partner.onReupClicked();
		// 	}
		// },

		/**
		 *signal that one of the active partners' period is up
		 * time to collect data & money
		 */
		get earned() { return this._(key).earned; },

		get totalIncome() {
			var partners = this._(key).partners;
			var length = partners.length;
			var sum = 0;
			for(var i = 0; i < length; i++) {
				sum += partners[i].earning;
			}
			return sum;
		},

		get backgroundTouched(){
			return this._(key).roomSpriteHandler.backgroundTouched;
		},

		get partnerAccepted(){ return this._(key).partnerAccepted; },
		get partnerCanceled(){ return this._(key).partnerCanceled; },

		get staffedChanged(){ return this._(key).staffedChanged; },
	};

	return PartnerHandler;
})();