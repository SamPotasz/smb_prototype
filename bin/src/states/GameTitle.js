var TITLE_BUTTON_POS = {"x":335, "y":453};
var TITLE_BUTTON_NAME = "playButton";


var GameTitle = function(game){
  this.preload = function() {
    console.log("loading web fonts");
    //  Load the Google WebFont Loader script
      game.load.script('webfont', '//ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js');
  };

  this.create = function(){
    console.log("Created!");
    var gameTitle = this.game.add.sprite(0,0,"titleBG");
    gameTitle.inputEnabled = true;
    gameTitle.events.onInputUp.add(this.playTheGame, this);

    if(SOUNDS.AMBIENT_SOUND){
      SOUNDS.AMBIENT_SOUND.stop();
    }

    // var playButton = this.game.add.button(TITLE_BUTTON_POS.x, TITLE_BUTTON_POS.y,
    //     STILL_ATLAS_NAME, this.playTheGame, this,
    //     TITLE_BUTTON_NAME, TITLE_BUTTON_NAME, TITLE_BUTTON_NAME, TITLE_BUTTON_NAME);
  };

  this.playTheGame = function(){
    this.game.state.start("TheGame");
  };

};