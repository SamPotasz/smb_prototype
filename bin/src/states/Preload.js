var Preload = function(game) {
	console.log("Initializing Preload state");
};

var SOUND_FILE_PREFIX = "assets/audio/";
var SOUND_FILE_SUFFIX = ".mp3";

var SOUND_SELECT_ROOM = {
	name: "SELECT_ROOM",
	file: "selectRoomUnlocked",
	loop: false,
};
var SOUND_SELECT_ROOM_LOCKED = {
	name: "SELECT_ROOM_LOCKED",
	file: "selectRoomLocked",
	loop: false,
};
var SOUND_UPGRADE_COMPLETE = {
	name: "UPGRADE_COMPLETE",
	file: "upgradeRoom",
	loop: false,
};
var SOUND_CLICK = {
	name: "CLICK",
	file: "selectButtonGeneral",
	loop: false,
};
var SOUND_WORKER_ADDED = {
	name: "WORKER_ADDED",
	file: "workerAddedToRoom",
	loop: false,
};
var SOUND_WORKER_REMOVED = {
	name: "WORKER_REMOVED",
	file: "workerRemovedFromRoom",
	loop: false,
};
var SOUND_HIRED = {
	name: "HIRED",
	file: "workerHired",
	loop: false,
};
var SOUND_FIRED = {
	name: "FIRED",
	file: "workerFired",
	loop: false,
};
var SOUND_ROOM_UNLOCKED = {
	name: "ROOM_UNLOCKED",
	file: "unlockRoom",
	loop: false,
};
var SOUND_BREACH = {
	name: "BREACH_SOUND",
	file: "Hit_Hurt15",
	loop: false,
};

var AMBIENT_SOUND = {
	name: "AMBIENT_SOUND",
	file: "office-ambience",
	loop: false,
};

var SOUND_LOADING_ARRAY = [
	SOUND_SELECT_ROOM,
	SOUND_SELECT_ROOM_LOCKED,
	SOUND_UPGRADE_COMPLETE,
	SOUND_CLICK,
	SOUND_WORKER_ADDED,
	SOUND_WORKER_REMOVED,
	SOUND_HIRED,
	SOUND_FIRED,
	SOUND_ROOM_UNLOCKED,
	SOUND_BREACH,
	AMBIENT_SOUND
];


Preload.prototype = {
	preload: function(){
		var loadingBar = this.add.sprite(160,240,"loading");
		loadingBar.anchor.setTo(0.5,0.5);
		this.load.setPreloadSprite(loadingBar);

		game.load.atlasJSONHash('stills', 'assets/textures/stills.png', 'assets/textures/stills.json');
		game.load.atlasJSONHash('animations', 'assets/textures/animations.png', 'assets/textures/animations.json');
		game.load.atlasJSONHash('workerAnims', 'assets/textures/workerAnims.png', 'assets/textures/workerAnims.json');
		game.load.image('background', 'assets/backgrounds_880/background.png');
		game.load.image('youWin', 'assets/backgrounds_880/gameoverWin.png');
		game.load.image('youLose', 'assets/backgrounds_880/gameoverLose.png');
		game.load.image('titleBG', 'assets/backgrounds_880/mainMenu.png');

		//load sounds
		var numSounds = SOUND_LOADING_ARRAY.length;
		for(var i = 0; i < numSounds; i++){
			var currSound = SOUND_LOADING_ARRAY[i];
			game.load.audio(currSound.name,
				SOUND_FILE_PREFIX + currSound.file + SOUND_FILE_SUFFIX);
		}
	},

  	create: function(){
		this.game.state.start("GameTitle");
	}
}