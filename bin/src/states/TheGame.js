var STILL_ATLAS_NAME = 'stills';
var ANIM_ATLAS_NAME = 'animations';
var WORKER_ANIM_ATLAS = 'workerAnims';

var FF_BUTTON_X = 278;
var FF_BUTTON_Y = 473;
var PLAY_BUTTON_X = 244;
var PLAY_BUTTON_Y = 471;
var RESTART_X = 313;
var RESTART_Y = 474;

var GAME_OVER_X = 0;
var GAME_OVER_Y = 0; //-200;
var GAME_OVER_TEXT_OFFSET_X = -157;
var GAME_OVER_TEXT_RIGHT_ALIGN = 559;
var GAME_OVER_TEXT_POSITIONS = [
	{
		left: {x: 483, y: 436},
		right: {x: 559, y: 420}
	},
	{
		left: {x: 486, y: 461},
		right: {x: 512, y: 445}
	},
	{
		left: {x: 488, y: 486},
		right: {x: 514, y: 470}
	},
	{
		left: {x: 490, y: 511},
		right: {x: 516, y: 495}
	},
	{
		left: {x: 492, y: 536},
		right: {x: 518, y: 520}
	},
];
var GAME_OVER_TEXT_ANGLE = -3.3;

var GAME_WIDTH = 880;
var GAME_HEIGHT = 660;
var ANIM_FPS = 30;

var FONT = {font: '14px Maven Pro', fill: 'white', stroke: 'white'};
var GRAY_BG_COLOR = "#343d3f";

var SOUNDS = {};

var TheGame = function(game){};

TheGame.prototype = {
	preload: function() {
		// game.add.plugin(Phaser.Plugin.Debug);

		var myData = smb;

		this.stats = new Stats(this.game, myData.constants.earningConstants, myData.pools, myData.constants.timing, myData.notifications);
		this.stats.pauseSignal.add(this.onStatPause, this);

		this.partnerHandler = new PartnerHandler(this.game, myData.partnerVars, this.stats);

		this.projects = [];
		this.preloadProjects(myData.projects, this.stats, 0, 0);

		this.levelHandler = new BusinessLevelHandler(this.game, myData.constants.earningConstants.businessLevels);
		this.breachController = new BreachController(this.game, this.stats, this.levelHandler, myData.constants.breachConstants);

		this.tutorial = new TutorialHandler(this.game, myData.tutorial);
	    this.tutorial.sceneChanged.add(this.onTutorialSceneChanged, this);
	    this.tutorial.tutorialFinished.add(this.onTutorialFinished, this);
	},

	preloadProjects: function(json, stats, gridLengthX, gridLengthY) {
		for(i = 0; i < json.length; i++){
			if(Stats.PARTNERS_ON || json[i].category != "partners") {
				var newProject = new Project(this.game, stats, json[i], gridLengthX, gridLengthY);
				this.projects.push(newProject);
			}
		}
	},

	create: function() {
		var numSounds = SOUND_LOADING_ARRAY.length;
		for(var i = 0; i < numSounds; i++) {
			var currLoadingObj = SOUND_LOADING_ARRAY[i];
			SOUNDS[currLoadingObj.name] = game.add.audio(currLoadingObj.name);
		}

	    this.game.stage.backgroundColor = '#FFFFFF';

		var background = game.add.sprite(0,0, 'background');
		background.events.onInputDown.add(this.onBackgroundTouched, this);
		background.inputEnabled = true;

	    for(i = 0; i < this.projects.length; i++) {
	    	this.createProject(this.projects[i]);
	    }

	    this.partnerHandler.create();
	    this.partnerHandler.partnerAccepted.add(this.onPartnerAccepted, this);
	    this.partnerHandler.partnerCanceled.add(this.onPartnerCanceled, this);
	    this.partnerHandler.earned.add(this.onPartnerEarned, this);
	    // this.partnerHandler.staffedChanged.add(this.onPartnerStaffingChanged, this);

	    this.levelHandler.create();
	    this.levelHandler.upgraded.add(this.onLevelUpgraded, this);

	    this.stats.create();
	    this.stats.ticked.add(this.onTicked, this);
	    this.stats.gameOver.add(this.onGameOver, this);
	    this.stats.periodTicked.add(this.onPayPeriod, this);

	    this.restartButton = this.game.add.button(RESTART_X, RESTART_Y, STILL_ATLAS_NAME, this.restartClickListener, this,
	    	'icon_restart', 'icon_restart', 'icon_restart', 'icon_restart');
	    this.ffButton = 	this.game.add.button(FF_BUTTON_X, FF_BUTTON_Y, STILL_ATLAS_NAME,  this.ffButtonListener, this,
	    	'icon_fastForward','icon_fastForward','icon_fastForward', 'icon_fastForward');
	    this.playButton= this.game.add.button(PLAY_BUTTON_X, PLAY_BUTTON_Y, STILL_ATLAS_NAME, this.playButtonListener, this,
	    	'icon_play', 'icon_play', 'icon_play', 'icon_play');

	    this.gameOverSprite = null;


	    this.breachController.create();
	    this.breachController.breached.add(this.onBreached, this);
	    this.breachController.continued.add(this.onContinued, this);

	    this.tutorial.create();

	    this.game.world.sendToBack(background);

	    SOUNDS.AMBIENT_SOUND.loopFull(0.5);
	},

	restartClickListener: function() {
		console.log("Restart clicked.");
		// this.resetStats();

		this.game.state.start("GameTitle");
	},

	ffButtonListener: function() {
		this.stats.onFastForward();
	},
	playButtonListener: function() {
		this.stats.onPlay();
	},

	createProject: function(project) {
		project.create();
		project.addClicked.add(this.onAddClicked, this);
		project.removeClicked.add(this.onRemoveClicked, this);
		project.startedSignal.add(this.onProjectStarted, this);
		project.upgradeHandler.startedSignal.add(this.onUpgradeStarted, this);
		project.upgradeHandler.canceledSignal.add(this.onUpgradeCanceled, this);
		project.globalUpgrade.add(this.onGlobalUpgrade, this);
		project.backgroundTouched.add(this.onProjectBackgroundTouched, this);
	},

	update: function() {
		this.stats.update();
	},

	onTicked: function() {
		// console.log("tick1");
		if(this.tutorial.isOn) {
			this.tutorial.onTicked();
		}
		if(this.tutorial.isOn && this.tutorial.isPaused) {
			return;
		}

		var earning = 0;
		var numProjects = this.projects.length;
		// console.log("numProjects: " + numProjects);
		for (var i = 0; i < numProjects; i++) {
			var currProj = this.projects[i];

			var currEarning = currProj.getEarning();
			earning += currEarning;

			currProj.onTicked();
			if(currProj.isCompleted) {
				if(currProj.effectTarget === undefined) {
					if(!currProj.isImmediate){
						this.stats.addEffects(currProj.effects, currProj);
					}
					this.breachController.updatePeriod(currProj);
				}
				else {
					//we're upgrading another project. find it and update its stats
					for(var j = 0; j < this.projects.length; j++) {
						var toUpdate = this.projects[j];
						if(toUpdate.key == currProj.effectTarget) {
							for(var name in currProj.effects) {
								var adding = currProj.effects[name];
								toUpdate[name] += adding;
							}
						}
					}
				}
				currProj.onCompleted();
				if(currProj.popsOffWorkers) {
					this.removeWorkersFromProject(currProj);
				}
			}
		}
		// console.log("total earnings)

		for(var k=0; k < this.projects.length; k++) {
			var currProj = this.projects[k];
			var wasLocked = currProj.locked;
			currProj.toggleLocked();
			if(!wasLocked && currProj.locked) {
				// console.log(currProj.projectType + " was locked.");
				this.removeWorkersFromProject(currProj);
			}
		}

		this.stats.onTicked();
		this.stats.updateIncomeDisplay(earning, this.partnerHandler.totalIncome);
		this.breachController.onTicked();
		this.partnerHandler.onTicked();
		this.levelHandler.onTicked(this.stats.cash);
	},

	onPartnerEarned: function(effects) {
		this.stats.addEffects(effects, undefined);
	},

	onLevelUpgraded: function() {
		this.stats.incrementProductScore();
	},

	removeWorkersFromProject: function(project) {
		var projWorkers = Math.min(project.numWorkers, project.maxWorkers);
		// console.log("removing " + projWorkers);
		var workerTypeIndices = project.workerTypeIndices;
		// console.log(workerTypeIndices);
		for(var i = 0; i < projWorkers; i++) {
			var toRemoveIndex = workerTypeIndices[i];
			// console.log("removing worker type " + toRemoveIndex)
			this.stats.onWorkerRemoved(toRemoveIndex);
		}
		project.numWorkers = 0;

		project.updateWorkersDisplay();
	},

	onPayPeriod: function() {
		for (i = 0; i < this.projects.length; i++) {
			this.projects[i].toggleLocked();
		}
	},

	/**
	 * stats to display:
	 *  Total revenue
	 *	Total money lost to breaches
	 *	Total salary paid
	 */
	onGameOver: function(youWin) {
		var spriteName = youWin ? 'youWin' : 'youLose';
		this.gameOverSprite = game.add.sprite(GAME_OVER_X, GAME_OVER_Y, spriteName);

		this.addGameOverTextRow(0, "TOTAL MONEY EARNED: ", toKDollars(this.stats.revenue));
		this.addGameOverTextRow(1, "LOST TO BREACHES: ", toKDollars(this.breachController.totalCashLost));
		this.addGameOverTextRow(2, "SALARY PAID: ", toKDollars(this.stats.totalSalaryPaid));
		this.addGameOverTextRow(3, "SPENT ON UPGRADES: ", toKDollars(this.stats.spentOnUpgrades));
		this.addGameOverTextRow(4, "FINAL PROFIT: ", toKDollars(this.stats.profit));

		this.gameOverSprite.inputEnabled = true;
		this.gameOverSprite.events.onInputDown.add(this.onGameOverClick, this);

		// this.stats.inputEnabled = false;
		// this.setProjectsInputEnabled(false);
	},

	addGameOverTextRow: function(rowIndex, label, stat) {
		var labelText = game.add.text(
			GAME_OVER_TEXT_POSITIONS[rowIndex].left.x + GAME_OVER_TEXT_OFFSET_X,
			GAME_OVER_TEXT_POSITIONS[rowIndex].left.y,
			label, FONT);
		labelText.angle = -3.3;
		labelText.wordWrap = false;
		this.gameOverSprite.addChild(labelText);

		var statText = game.add.text(
			GAME_OVER_TEXT_POSITIONS[rowIndex].right.x + GAME_OVER_TEXT_OFFSET_X,
			GAME_OVER_TEXT_POSITIONS[rowIndex].right.y,
			stat, FONT);
		statText.x = GAME_OVER_TEXT_RIGHT_ALIGN + rowIndex*2 - statText.width;
		statText.angle = -3.3;
		statText.wordWrap = false;
		this.gameOverSprite.addChild(statText);
	},

	onGameOverClick: function() {
		game.state.start('TheGame', true, false);
	},

	//try to add a worker to a timer
	onAddClicked: function(workTimer, project) {
 		if(workTimer.canAddWorker){
 			if(workTimer.maxWorkers == 0) {
 				if(project.isImmediate){
 					this.stats.addEffects(project.effects, project);
 					this.stats.updateDisplays();
				}
				project.addWorker(this.stats.newestWorkerIndex);

			}
			else if(this.stats.freeWorkers > 0) {
				project.addWorker(this.stats.newestWorkerIndex);
				this.stats.onWorkerAdded();
			}


			this.updateSecurityStats(project, true);
		}
		else {
			console.log("Can't add worker.");
		}
	},

	//listener for project dispatching start signal
	onProjectStarted: function(project) {
		this.stats.onProjectStart(project);
	},

	/*
	 * listener for project dispatching
	 * signal that remove worker button was clicked
	 */
	onRemoveClicked: function(fromProject, workerType) {
		if(fromProject.projectTimer.maxWorkers > 0){
			this.stats.onWorkerRemoved(workerType);
		}

		this.updateSecurityStats(fromProject, false);
	},

	updateSecurityStats: function(project, isOn) {
		if(project.key == "dataSecurity"){
			this.stats.dataSecurityIsOn = isOn;
		}
		else if(project.key == "employeeSecurity") {
			this.stats.employeeSecurityIsOn = isOn;
		}
		console.log("DS is on? " + this.stats.dataSecurityIsOn);
		console.log("ES is on? " + this.stats.employeeSecurityIsOn);
	},

	/**
	 * Listener for an upgradeHandler dispatching its started signal
	 * @param handler: the upgradeHandler which dispatched the signal
	 */
	onUpgradeStarted: function(handler) {
		var workersNeeded = handler.workersNeeded;
		var cost = handler.cost;
		var timer = handler.workTimer;
		// console.log("Starting upgrade needing " + workersNeeded + " workers and " + "$" + cost);
		//double check that we have the resources
		if((this.stats.cash >= cost || cost <= 0) && this.stats.freeWorkers >= workersNeeded) {
			// this.stats.cash -= cost;
			this.stats.onUpgradeStarted(cost);
			var workerTypeIndices = [];
			//move workers to upgrade
			for(var i = 0; i < workersNeeded; i++) {
				workerTypeIndices.push(this.stats.newestWorkerIndex);
				this.stats.onWorkerAdded();
				timer.incrementWorkers();
			}
		}
	},

	/**
	 * Listener for upgrade stopping work.
	 * Dispatched upon completion and on cancel button clicked
	 */
	onUpgradeCanceled: function(handler, workerTypeIndices) {
		var numWorkers = handler.workTimer.numWorkers;
		for(var i = 0; i < numWorkers; i++) {
			this.stats.onWorkerRemoved(workerTypeIndices[i]);
			handler.workTimer.decrementWorkers();
		}
	},

	/**
	 * dispatched when an upgrade finishes and it
	 * affects global this.stats.
	 * encryption and employeeSecurity fit here
	 */
	onGlobalUpgrade: function(effect) {
		console.log("Global upgrade!");
		var object = {};
		object[effect.key] = effect.value;
		this.stats.addProperty(object, effect.key);
		this.stats.updateDisplays();
		// console.log("Securities: " + this.stats.dataSecurity + " / " + this.stats.employeeSecurity);
	},

	onPartnerAccepted: function(newPartner) {
		console.log("accepted partner for " + newPartner.partnerStat);
		// partners.push(newPartner);
		var numProjects = this.projects.length;
		for(var i = 0; i < numProjects; i++) {
			var currProj = this.projects[i];
			if(currProj.key == newPartner.partnerStat) {
				// this.partnerHandler.addPartner(newPartner, currProj);
				this.breachController.addPartner(newPartner);
			}
		}
	},

	onPartnerCanceled: function(removedPartner) {
		// console.log("Partner canceled");
		this.breachController.removePartner(removedPartner);
	},

	onTutorialSceneChanged: function(allowedProjects, toWatch, inGameScene) {
		//enable input on projects
		// console.log("Scene changed. Looking to watch ");
		// console.log(toWatch);

		var numAllowed = allowedProjects.length;
		var numProjects = this.projects.length;

		for(var i = 0; i < numProjects; i++) {
			var currProject = this.projects[i];
			//if we're in the game, all projects should be enabled
			currProject.inputEnabled = inGameScene;
			if(!currProject.inputEnabled) {
				currProject.onBackgroundTouched(false);
			}
			for(var j = 0; j < numAllowed; j++) {
				var currCheck = allowedProjects[j];
				if(currCheck == currProject.key) {
					// console.log("Matched on " + currProject.key);
					currProject.inputEnabled = true;
				}
			}
			if(currProject.key == toWatch) {
				this.tutorial.watchProject(currProject);
			}
		}

		//watch projects

		if(toWatch == "stats") {
			this.tutorial.watchProject(this.stats);
		}

		// console.log("setting inputEnabled on stats to " + inGameScene);
		this.stats.inputEnabled = inGameScene; //false;
	},

	/**
	 * Sets inputEnabled value on all projects to given value
	 */
	setProjectsInputEnabled: function(value) {
		var numProjects = this.projects.length;
		for(var i = 0; i < numProjects; i++) {
			var currProject = this.projects[i];
			currProject.inputEnabled = value;
			// if(!value) {
			// 	curr
			// }
		}
	},

	onTutorialFinished: function() {
		console.log("We finished the tutorial!");
		// this.restartClickListener();
		this.resetStats();
	},

	resetStats: function() {
		this.stats.onGameRestart(); //constants);

		for(i = 0; i < this.projects.length; i++) {
			var currProj = this.projects[i];
			currProj.onGameRestart(this.stats);
		}

		if(this.gameOverSprite && this.gameOverSprite.exists) {
			this.gameOverSprite.destroy();
		}

		this.breachController.onGameRestart();

		this.partnerHandler.onGameRestart();
	},

	/**
	 * listener for breach timer going off
	 */
	onBreached: function() {
		this.stats.onBreached();
		// this.setProjectsInputEnabled(false);
		this.stats.inputEnabled = false;
	},

	onContinued: function() {
		this.stats.onContinued();
		// this.setProjectsInputEnabled(true);
	},

	onProjectBackgroundTouched: function(projectKey) {
		var numProjects = this.projects.length;
		for(var i = 0; i < numProjects; i++) {
			var currProject = this.projects[i];
			if(currProject.key != projectKey){
				currProject.hidePopup();
			}
		}
		// this.stats.hidePoolPopups();
		this.partnerHandler.hidePopups();
	},

	//when user clicks the background of the game
	onBackgroundTouched: function() {
		var numProjects = this.projects.length;
		for(var i = 0; i < numProjects; i++) {
			this.projects[i].hidePopup();
		}

		this.partnerHandler.hidePopups();
	},

/*
	onPoolTouched: function(statName) {
		// this.stats.hidePoolPopups(statName);

		var numProjects = this.projects.length;
		for(var i = 0; i < numProjects; i++) {
			this.projects[i].hidePopup();
		}
	},
*/
	onStatPause: function(paused) {
		var enabled = !paused;
		this.setProjectsInputEnabled(enabled);
		this.stats.inputEnabled = enabled;
	},
}