var CONSOLE_GRID_OFFSET_X = 0;
var CONSOLE_GRID_OFFSET_Y = 3;

// var TIMER_ASSET_PREFIX = "animations/loadingBar/loadingBar_";
var TIMER_FOLDER_PREFIX = "projectProgress_";
var TIMER_FILE_PREFIX = "projectProgress_";
var NUM_TIMER_FRAMES = 100;
var TIMER_OFFSET_X = 50;
var TIMER_OFFSET_Y = 30;
var TIMER_ANIM_NAME = 'idle';

TimerDisplay = (function(){

	var key = {};

	function TimerDisplay(gameParam, consoleNameParam, timerDirectionParam, hasMonitorParam) {
		var $ = {
			game: gameParam,
			consoleSprite: undefined,
			timerSprite: undefined,
			hasMonitor: hasMonitorParam,

			consoleName: consoleNameParam,
			timerDirection: timerDirectionParam,
		};

		this._ = function(aKey) {
			if(aKey === key)
	        	return $;

	    	console.trace();
	      	throw new Error("The _() function must be called internally.");
		}
	}

	TimerDisplay.prototype = {
		create: function(monitorX, monitorY) {
			var $ = this._(key);

			if($.hasMonitor){
				$.consoleSprite = $.game.add.sprite(monitorX, monitorY,
					STILL_ATLAS_NAME, $.consoleName);
			}

			var timerX = monitorX;
			var timerY = monitorY;
			if($.timerDirection == "West") {
				timerX += 7;
			}

			var timerName = TIMER_FOLDER_PREFIX + $.timerDirection.toLowerCase() + "/" +
				TIMER_FILE_PREFIX + $.timerDirection.toLowerCase() + "_";
			$.timerSprite = $.game.add.sprite(timerX,	timerY,
					ANIM_ATLAS_NAME, timerName + '0');
		},

		updateTimeBar: function(timeLeft, maxTime) {
			// console.log("TimeLeft & MaxTime: " + timeLeft + ", " + maxTime);
			var $ = this._(key);
			var percentageComplete = (maxTime - timeLeft) / maxTime;
			var frameNumber = Math.floor(percentageComplete * NUM_TIMER_FRAMES);
			//99 is our max frame number in the animation. So cap it.
			var frameNumber = Math.min(99, frameNumber);
			var frameTitle = TIMER_FOLDER_PREFIX + $.timerDirection.toLowerCase() + "/" +
				TIMER_FILE_PREFIX + $.timerDirection.toLowerCase() + "_" + frameNumber;
			$.timerSprite.frameName = frameTitle;
		},

		showLocked: function(locked) {
			var $ = this._(key);
			if($.hasMonitor){
				$.consoleSprite.visible = !locked;
			}
			$.timerSprite.visible = !locked;
		},
	};

	return TimerDisplay;
})();