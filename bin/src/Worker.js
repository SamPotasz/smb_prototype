Worker = (function(){

	var key = {};

	function Worker(gameParam, xParam, yParam) {
		var $ = {
			game: gameParam,
			sprite: undefined,
			x: xParam,
			y: yParam
		};

		this._ = function(aKey) {
			if(aKey === key)
	        	return $;

	    	console.trace();
	      	throw new Error("The _() function must be called internally.");
		}
	}

	Worker.prototype = {
		create: function(){
			var $ = this._(key);
			$.sprite = new Sprite($.game, $.x, $.y, 'workerIcon');
			sprite.inputEnabled = true;
			sprite.input.enableDrag();
			sprite.events.onDragStart.add(this.onWorkerDragStart, this);
			sprite.events.onDragStop.add(this.onWorkerDragStop, this);
		}

		get sprite(){ return this._(key).sprite; },
	};

	return Worker;
})();
