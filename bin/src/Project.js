var RATE_PER_WORKER = 1.0;

var POPUP_DURATION = 500;
var POPUP_Y_START = 7;
var POPUP_X_START = 80;
var POPUP_TWEEN_HEIGHT = 7;

var COST_DISPLAY_PREFIX = "STARTUP COST:\t";
var PROJECT_LINE_HEIGHT = 26;
var MINUS_BUTTON_LINE_BONUS = 8;

// var LEVEL_DOTS_Y_OFFSET = 35;
// var LEVEL_DOTS_PREFIX = "progressDots_";
var LEVEL_MONITOR_NAME = "monitor_securityLevel";

// var EMPLOYEE_OPTIONS_Y = 70;
var ADD_BUTTON_X = 123;
var ADD_BUTTON_NAME = 'buttonPopupPlus';
var MINUS_BUTTON_X = 16;
var MINUS_BUTTON_NAME = 'buttonPopupMinus';
var POPUP_WORKER_X = 90;
var POPUP_WORKER_NAME = 'popupWorker';

Project = (function() {
	var key = {};

	Project = function(gameParam, statsParam, json){
		var $ = {
			game : gameParam,
			stats : statsParam,
			projectTimer : new WorkTimer(json.maxWorkers, json.levels[0].duration),
			upgradeHandler : new UpgradeHandler(game, json.upgrades, json.artVars.monitor, json.artVars.upgradeWorkers),

			roomSpriteHandler : new RoomSpriteHandler(game, json.artVars, json.key, json.moreInfo),
			workersDisplay : new WorkersDisplay(game, json.deskVars, json.key),

			monitorX: json.artVars.monitor.x,
			monitorY: json.artVars.monitor.y,
			timerDisplay : null,

			levelMonitorPos: json.artVars.levelMonitor,
			levelMonitor: null,
			levelDisplay: null,

			oneClickButtonVars: json.artVars.oneClickButton,

			productScore : 0,
			cash : 0,
			efficiency : 0,
			emails : 0,
			credentials : 0,
			creditCards : 0,
			advertising : 0,
			newEmployees : 0,
			income : 0,
			ITSecurity : 0,
			breachResponse : 0,
			levelNumber : 0,
			locked : false,

			projectType : json.projectType,
			moreInfo: json.moreInfo,
			key : json.key,
			category : json.category,
			popsOffWorkers: json.popsOffWorkers,
			effectTarget : json.effectTarget,
			partnerStat : json.partnerStat,
			levels : json.levels,
			isImmediate: json.key == "fire",

			addWorkerLabel: json.artVars.addWorkerLabel,
			removeWorkerLabel: json.artVars.removeWorkerLabel,

			addClicked : new Phaser.Signal(),
			removeClicked : new Phaser.Signal(),
			//when an upgrade affects global stats (i.e. security)
			globalUpgrade : new Phaser.Signal(),
			started : new Phaser.Signal(),
			backgroundTouched : new Phaser.Signal(),

			workerDragStopped : new Phaser.Signal(),
			partnerCanceled: new Phaser.Signal(),
		}

		$.timerDisplay = new TimerDisplay(game,
			"monitor_" + json.key,
			json.artVars.monitor.direction,
			!($.key == "hire" || $.key == "fire"));

		this._ = function(aKey) {
		    if(aKey === key)
		    	return $;

		    console.trace();
		    throw new Error("The _() function must be called internally.");
		}

		//for some reason, putting this in prototype messes up this._(key)
		this.onUpgradeComplete = function(upgradeEffects) {
			for(var i = 0; i < upgradeEffects.length; i++) {
				var effect = upgradeEffects[i];
				var key = effect.key;
				var value = effect.value;
				// console.log("adding " + value + " to " + key);
				// console.log(this);
				if(key == "maxWorkers") {
					$.projectTimer.increaseMaxWorkers(value);
				}
				else if(key == "dataSecurity" || key == "employeeSecurity") {
					$.globalUpgrade.dispatch(effect);
				}
				else {
					$.effects[key] += value;
				}
				this.resetDisplays();
			}
		};
	}

	Project.prototype = {

	   	create: function () {
			//set up timers
			this.setLevelStats();

			var $ = this._(key);

			// if($.timerDisplay != null){
			if(this.hasOneClickButton){
				$.timerDisplay.create($.oneClickButtonVars.x,
					$.oneClickButtonVars.y);
			}
			else {
				$.timerDisplay.create($.monitorX, $.monitorY);
			}


			if(this.hasOneClickButton) {
				var vars = $.oneClickButtonVars;
				$.oneClickButton = $.game.add.button(vars.x, vars.y,
					STILL_ATLAS_NAME, this.onOneClickButtonClicked, this,
					vars.name, vars.name, vars.name, vars.name);
			}
			else {
				$.roomSpriteHandler.create();
				$.roomSpriteHandler.backgroundTouched.add(this.onBackgroundTouched, this);
				var bgWidth = $.roomSpriteHandler.popupBackgroundWidth;

				$.projectText = $.game.add.text(0, PROJECT_LINE_HEIGHT*0,
				$.moreInfo, FONT);
				$.projectText.wordWrap = true;
				$.projectText.wordWrapWidth = bgWidth - 10;
				$.projectText.lineSpacing = -3;
				$.needText = $.game.add.text(0, PROJECT_LINE_HEIGHT * 1, "", FONT);
				//shows up behind the add worker button when button's unavailable
				// $.addWorkerText = $.game.add.text(0, PROJECT_LINE_HEIGHT * 2, "", FONT);

				var employeeOptionsY = $.roomSpriteHandler.popupBackgroundHeight - 60;
				$.addButton = $.game.add.button(ADD_BUTTON_X, employeeOptionsY,
					STILL_ATLAS_NAME, this.addWorkerListener, this,
					ADD_BUTTON_NAME, ADD_BUTTON_NAME, ADD_BUTTON_NAME, ADD_BUTTON_NAME);
				// labelButton($.addButton, game, $.addWorkerLabel, FONT);

				var minusY = $.projectTimer.maxWorkers > 1 ?
					PROJECT_LINE_HEIGHT * 3 + MINUS_BUTTON_LINE_BONUS : $.addButton.y;
				$.minusButton = $.game.add.button(MINUS_BUTTON_X, employeeOptionsY,
					STILL_ATLAS_NAME, this.removeWorkerListener, this,
					MINUS_BUTTON_NAME, MINUS_BUTTON_NAME, MINUS_BUTTON_NAME, MINUS_BUTTON_NAME);
				// labelButton($.minusButton, game, $.removeWorkerLabel, FONT);

				$.workerIcon = $.game.add.sprite(POPUP_WORKER_X, employeeOptionsY,
					STILL_ATLAS_NAME, POPUP_WORKER_NAME);

				var textGroup = $.game.add.group();
				textGroup.add($.projectText);
				textGroup.add($.needText);
				textGroup.add($.addButton);
				// textGroup.add($.addWorkerText);
				textGroup.add($.minusButton);
				textGroup.add($.workerIcon);

				centerXWithin($.projectText, bgWidth);
				centerXWithin($.needText, bgWidth);

				$.roomSpriteHandler.addGroup(textGroup);
			}

			if($.key == "employeeSecurity" || $.key == "dataSecurity"){
				$.levelMonitor = $.game.add.sprite($.levelMonitorPos.x,
					$.levelMonitorPos.y,
					STILL_ATLAS_NAME, LEVEL_MONITOR_NAME);
				$.levelLabel = $.game.add.text(
					$.levelMonitorPos.x + 5,
					$.levelMonitorPos.y + 3, "LEVEL", FONT);
				$.levelLabel.angle = 3;
				// centerXWithin(label, $.levelMonitor);

				$.levelDisplay = $.game.add.text(
					$.levelMonitorPos.x + 20,
					$.levelMonitorPos.y + 17, "0", FONT);
				$.levelDisplay.angle = 3;
				// centerXWithin($.levelDisplay, $.levelMonitor);

				// $.levelMonitor.addChild(label);
				// $.levelMonitor.addChild($.levelDisplay);
			}

			//create upgradeHandler
			if($.upgradeHandler.hasUpgrades) {
				$.upgradeHandler.create($.roomSpriteHandler.popupPosition);
				$.upgradeHandler.completedSignal.add(this.onUpgradeComplete, this);
				$.upgradeHandler.toggleLocked($.stats);
				$.upgradeHandler.backgroundTouched.add(this.onBackgroundTouched, this);
			}

			$.workersDisplay.create(
				{x: $.roomSpriteHandler.x, y: $.roomSpriteHandler.y},
				$.projectTimer.maxWorkers);

			this.toggleLocked();
			this.resetDisplays();
		},

		setLevelStats : function() {
			var $ = this._(key);

			var currLevel = $.levels[$.levelNumber];
			$.cost = currLevel.cost;
			$.timeToComplete = currLevel.duration;
			$.initialTime = $.timeToComplete;
			// this.timeLeft = this.timeToComplete;

			// console.log("TimeLeft for level " + this.levelNumber +": " + this.timeLeft);

			$.effects = {};
			for(var i = 0; i < currLevel.effects.length; i++) {
				var effect = currLevel.effects[i];
				// console.log("effect of " + this.projectType + ": +" + effect.value + " " + effect.key);
				$.effects[effect.key] = effect.value;
			}
			// console.log("target for " + this.key + ": " + this.effects["target"]);

			$.prereqs = currLevel.prereqs;
		},

		/**
		 * TODO: don't do this every tick. or at least not
		 * check for upgrades every tick
		 */
		toggleLocked: function() {
			var $ = this._(key);

			var wasLocked = $.locked;
			$.locked = this.checkLocked();

			if(wasLocked != $.locked) {
				$.roomSpriteHandler.showLocked($.locked);
				$.workersDisplay.showLocked($.locked);
				if($.timerDisplay != null){
					var showLocked = $.locked;
					if(this.hasOneClickButton){
						showLocked = $.locked && $.projectTimer.numWorkers == 0;
					}
					$.timerDisplay.showLocked(showLocked);
				}
				if($.levelMonitor != null){
					$.levelMonitor.visible = !$.locked;
					$.levelLabel.visible = !$.locked;
					$.levelDisplay.visible = !$.locked;
				}
				if(!this.hasOneClickButton) {
					$.workerIcon.visible = !$.locked;
					$.projectText.visible = !$.locked;
				}
				this.resetDisplays();

				if(!$.locked) {
					playSound(SOUND_ROOM_UNLOCKED);
				}
			}

			//check for upgrade being unlocked
			if(!$.locked) {
				$.upgradeHandler.toggleLocked($.stats, $.projectType == "Sales Center");
			}
		},

		checkLocked : function() {
			var $ = this._(key);

			var prereqLocked = !prereqsMet($.prereqs, $.stats);

			var locked = prereqLocked;

			if(!locked) {
				locked = (this.hasLevels && this.pastLastLevel()) ||
					($.cost > 0 && $.cost > $.stats.cash &&
						$.projectTimer.timeLeft == $.timeToComplete);
			}

			return locked;
		},

		resetDisplays : function() {
			this.updateWorkersDisplay();
			this.updateTimeDisplays();
			// $.roomSpriteHandler.setTimeText(this.getTimeLeftString());
			this.setNeedText();
			this.setLevelMonitors();
		},

		setLevelMonitors: function() {
			var $ = this._(key);
			if($.levelMonitor != null) {
				if($.projectTimer.numWorkers > 0) {
					var levelNumber = $.upgradeHandler.upgradesCompleted;
					$.levelDisplay.setText(levelNumber);
				}
				else {
					$.levelDisplay.setText("OFF");
				}
			}
		},

		setButtonVisibilities: function() {
			var $ = this._(key);

			if(this.hasOneClickButton){
				$.oneClickButton.visible = !$.locked && $.projectTimer.numWorkers == 0;
				return;
			}

			var unlocked = !$.locked;
			$.minusButton.visible = unlocked;
			$.minusButton.alpha = $.projectTimer.numWorkers > 0 ? 1.0 : 0.4;

			$.addButton.visible = unlocked;
			var canAddWorker = $.projectTimer.canAddWorker;
			if(this.isSecurityProject) {
				canAddWorker = $.projectTimer.numWorkers < $.projectTimer.maxWorkers;
			}
			var buttonActive = canAddWorker && $.stats.freeWorkers;
			$.addButton.inputEnabled = buttonActive;
			$.addButton.alpha = buttonActive > 0 ? 1.0 : 0.4;
		},

		setNeedText : function() {
			var $ = this._(key);
			if(isUndefined($.needText)) {
				return;
			}

			var string;

			if($.locked) {
				var prereqString = "Need";
				var numPreqs = $.prereqs.length;
				for(var i = 0; i < numPreqs; i++) {
					if(i > 0) {
						prereqString += "\nand ";
					}

					var prereq = $.prereqs[i];
					var suffix = prereq.key + "s";
					var needed = prereq.value - $.stats[prereq.key];

					if(prereq.key == "totalData") {
						suffix = "pieces of data collected."
					}
					else if(prereq.key == "totalEmails") {
						suffix = "email addresses collected."
					}

					prereqString += " " + needed + " " + suffix;

					if(prereq.key == "productScore"){
						prereqString = "Need Business Level of " + (needed + 1);
					}
				}
				string = prereqString;
				if($.cost > $.cash) {
					string = "Need " + toKDollars($.cost);
				}
			}
			else {
				if($.cash > 0) {
					string = "Earns: " + this.dollarsPerSalesCall();
				}
				else if($.cost > 0) {
					string = "Cost: " + $.cost;
				}
				else {
					string = "";
				}
			}
			$.needText.setText(string);
			centerXWithin($.needText, $.roomSpriteHandler.popupBackgroundWidth);
		},

		updateTimeDisplays: function() {
			var $ = this._(key);
			if($.timerDisplay != null){
				$.timerDisplay.updateTimeBar($.projectTimer.timeLeft, $.projectTimer.maxTime);

				if(this.hasOneClickButton){
					// console.log("oneClickButton. Locked? " + $.locked + " Workers: " + $.projectTimer.numWorkers + " Time Left: " + $.projectTimer.timeLeft);
					showLocked = $.locked && $.projectTimer.numWorkers == 0;
					$.timerDisplay.showLocked(showLocked);
				}
			}
		},

		dollarsPerSalesCall : function() {
			var $ = this._(key);

			return $.effects.cash * $.stats.efficiency;
		},

		onOneClickButtonClicked: function() {
			console.log("clicked 1cb");
			this.addWorkerListener();
		},

		addWorkerListener : function(){
			var $ = this._(key);

			$.addClicked.dispatch($.projectTimer, this);
		},

		removeWorkerListener : function() {
			var $ = this._(key);

			var timer = $.projectTimer;
			if(timer.numWorkers > 0){
				timer.decrementWorkers();
				this.removeClicked.dispatch(this, $.workersDisplay.lastWorkerType);
				this.updateWorkersDisplay();

				playSound(SOUND_WORKER_REMOVED);
			}
			this.setLevelMonitors();
		},

		addWorker : function(newWorkerIndex){
			var $ = this._(key);
			$.projectTimer.incrementWorkers();
			this.updateWorkersDisplay(newWorkerIndex);

			if($.key != "fire") {
				playSound(SOUND_WORKER_ADDED);
			}

			this.setLevelMonitors();
		},

		updateWorkersDisplay : function(newWorkerIndex) {
			var $ = this._(key);

			var timer = this._(key).projectTimer;
			$.workersDisplay.updateDisplay(timer.numWorkers, timer.maxWorkers, newWorkerIndex);
		},

		//each tick of the clock, progress towards completion
		onTicked : function() {
			var $ = this._(key);
			// console.log($.projectType + " ticked.");

			var timer = $.projectTimer;
			if((timer.numWorkers > 0 && !$.locked) || this.hasOneClickButton){
				if(timer.timeLeft == timer.maxTime) {
					this.onStarted();
				}
				timer.onTicked();
				this.updateTimeDisplays();
			}

			if($.upgradeHandler.hasUpgrades) {
				// console.log(this.projectType + " has upgrades.");
				$.upgradeHandler.onTicked();
			}
			this.setButtonVisibilities();
		},

		onStarted : function() {
			var $ = this._(key);

			if($.cost > 0) {
				this.showPopup("-$" + $.cost);
			}
			$.started.dispatch(this);
		},

		onCompleted : function() {
			this.levelUp();
			this._(key).projectTimer.onCompleted();
			this.resetDisplays();
		},

		//for projects that have levels, increase stats upon completion
		levelUp : function() {
			var $ = this._(key);

			if(this.hasLevels){
				if(!$.upgradeHandler.hasUpgrades) {
					$.levelNumber++;
					if(!this.pastLastLevel()) {
						this.setLevelStats();
					}
					// $.levelDots.frameName = LEVEL_DOTS_PREFIX +
					// 	$.key + "_" + $.levelNumber;
				}
			}
		},

		get hasLevels() {
			return this._(key).levels.length > 1;
		},
		hasLevelsLeft : function() {
			var $ = this._(key);

			return $.levelNumber < $.levels.length - 1;
		},
		pastLastLevel : function() {
			var $ = this._(key);

			var toReturn = $.levelNumber >= $.levels.length;
			return toReturn;
		},

		/**
		 * show a little bing when we earn some stats
		 */
		showPopup : function(popString) {
			var $ = this._(key);

			var yStart;
			var xStart;
			yStart = $.roomSpriteHandler.y + POPUP_Y_START;
			var xOffset = (popString.substring(0,1)=="-") ? 0 : 50;
			xStart = $.roomSpriteHandler.x + POPUP_X_START + xOffset;

			globalPopup($.game, xStart, yStart, popString);
		},

		/**
		 * listener for the RESTART button being clicked.
		 * remove all workers and reset all stats to inial state
		 */
		onGameRestart : function(stats) {
			var $ = this._(key);

			var timer = $.projectTimer;
			timer.numWorkers = 0;

			$.levelNumber = 0;
			this.setLevelStats();

			timer.onGameRestart();

			$.upgradeHandler.onGameRestart(stats);

			// $.roomSpriteHandler.hidePopup();

			this.toggleLocked();
			this.resetDisplays();

			$.roomSpriteHandler.onGameRestart();
		},

		/**
		 * void -> bool
		 * whether or not we're allowed to add another worker to this project
		 */
		canAddWorker : function() {
			return $.projectTimer.canAddWorkers;
		},

		/**
		 * void -> int
		 */
		getEarning : function() {
			var $ = this._(key);

			if($.projectTimer.numWorkers > 0) {
				/**
				 *	$ / payPd = $/cyc * cyc/payPd
				 *	$ / pp = $/cyc * (t/pp) / (t/cyc)
				 */
				if($.effects.cash > 0) {
					var dollarsPerCall = this.dollarsPerSalesCall();
					// console.log("ticksPerCycle: " + ticksPerCycle);
					return dollarsPerCall * this.cyclesPerPayPeriod();
				}
				/**
				 * cyc/payPd * $/cyc
				 * cyc/pp * emails * $/email
				 */
				else if($.effects.advertising > 0) {
					var cycPerPp = this.cyclesPerPayPeriod();
					var dollarsPerCyc = $.stats.totalEmails * $.stats.dollarsPerEmail;
					var returning = cycPerPp * dollarsPerCyc * $.stats.press;
					// console.log("From emails: " + returning);
					return returning;
				}
			}
			return 0;
		},

		cyclesPerPayPeriod : function() {
			var $ = this._(key);

			var ticksPerCycle = $.timeToComplete / $.projectTimer.numWorkers;
			return $.stats.ticksPerPayPeriod / ticksPerCycle;
		},

		onBackgroundTouched: function(showPopup) {
			var $ = this._(key);

			$.upgradeHandler.togglePopup(showPopup);

			var soundEffect = $.locked ? SOUND_SELECT_ROOM_LOCKED : SOUND_SELECT_ROOM;
			playSound(soundEffect);

			$.backgroundTouched.dispatch($.key);
		},

		hidePopup: function() {
			var $ = this._(key);
			$.upgradeHandler.togglePopup(false);
			if(!this.hasOneClickButton){
				$.roomSpriteHandler.hidePopup();
			}
		},

		get addClicked() {
			return this._(key).addClicked;
		},
		get removeClicked() {
			return this._(key).removeClicked;
		},

		get upgradeHandler() {
			return this._(key).upgradeHandler;
		},

		get projectTimer() {
			return this._(key).projectTimer;
		},

		get numWorkers() {
			return this._(key).projectTimer.numWorkers;
		},
		set numWorkers(value) {
			this._(key).projectTimer.numWorkers = value;
		},

		get isCompleted() {
			return this._(key).projectTimer.isCompleted;
		},

		get startedSignal() {
			return this._(key).started;
		},

		get effects() {
			return this._(key).effects;
		},

		get cost() {
			return this._(key).cost;
		},

		get key() { return this._(key).key; },

		get x() { return this._(key).roomSpriteHandler.x; },
		get y() { return this._(key).roomSpriteHandler.y; },
		get screenOrigin(){
			return this._(key).roomSpriteHandler.screenOrigin;
		},

		get globalUpgrade() { return this._(key).globalUpgrade; },

		get workerDragStopped(){ return this._(key).workerDragStopped; },

		/**
		 * Used to enable / disable input based on tutorial
		 */
		set inputEnabled(value) {
			var $ = this._(key);
			if(this.hasOneClickButton){
				$.oneClickButton.inputEnabled = value;
				console.log("1cb inputEnabled = " + $.oneClickButton.inputEnabled);
			}
			else {
				$.roomSpriteHandler.inputEnabled = value;
				$.addButton.inputEnabled = value;
				$.minusButton.inputEnabled = value;
			}
		},

		get popsOffWorkers(){
			return this._(key).popsOffWorkers || this.hasLevels;
		},

		get backgroundTouched(){ return this._(key).backgroundTouched; },

		get partnerCanceled(){ return this._(key).partnerCanceled; },

		get workerTypeIndices(){ return this._(key).workersDisplay.workerSpriteIndices; },

		get hasOneClickButton(){
			var $ = this._(key);
			return $.key == "hire" || $.key == "fire";
		},

		get maxWorkers(){
			return this._(key).projectTimer.maxWorkers;
		},

		get isImmediate(){ return this._(key).isImmediate; },

		get isSecurityProject(){ 
			return this._(key).key == "employeeSecurity" || this._(key).key == "dataSecurity"
		},
	}

	return Project;
})();