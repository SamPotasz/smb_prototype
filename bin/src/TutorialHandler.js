var TUTORIAL_START_ON = true;

var TUTORIAL_MODE_INTRO = "pre game-play tutorial";
var TUTORIAL_MODE_GAMEPLAY = "narrative events that happen in the game";

var TUT_BG_NAME = "box_tutorial";
var TUT_BG_X = 560;
var TUT_BG_Y = 480;
var TUT_BG_WIDTH = 320;
var TUT_BG_HEIGHT = 120;

var TUT_TEXT_X = 10;
var TUT_TEXT_Y = 4;
var TUT_TEXT_WIDTH = 300;

var SKIP_X = 10;
var SKIP_Y = 115;
var NEXT_X = 200;
var NEXT_Y = 115;

var TUTORIAL_STYLE = {font: '16px Maven Pro',
	fill: 'white', stroke: 'white',
	wordWrap: true, wordWrapWidth: TUT_TEXT_WIDTH};

TutorialHandler = (function(){

	var key = {};

	function TutorialHandler(gameParam, tutorialJson) {
		var $ = {
			game: gameParam,
			introScenes: tutorialJson,
			currSceneIndex: 0,
			watching: null,
			isOn: TUTORIAL_START_ON,

			popupGroup: undefined,
			textDisplay: undefined,
			highlightGroup: undefined,

			skipButton: undefined,
			nextButton: undefined,

			sceneChanged: new Phaser.Signal(),
			tutorialFinished : new Phaser.Signal(),
		};

		this._ = function(aKey) {
			if(aKey === key)
	        	return $;

	    	console.trace();
	      	throw new Error("The _() function must be called internally.");
		};

		this.displayCurrIntroScene = function() {
			var currScene = $.introScenes[$.currSceneIndex];
			$.highlightGroup.removeAll();

			var numHighlights = currScene.highlightSprites.length;
			for(var i = 0; i < numHighlights; i++) {
				var spriteVars = currScene.highlightSprites[i];
				// console.log("adding " + currScene.highlightSprite.assetName);
				var sprite = $.game.add.sprite(spriteVars.x, spriteVars.y,
					ANIM_ATLAS_NAME);//, spriteVars.assetName + "0");
				sprite.angle = spriteVars.rotation;
				var frames = Phaser.Animation.generateFrameNames(
					spriteVars.assetName, 0, 21);
				// console.log("Frames: ");
				// console.log(frames);
				sprite.animations.add("123456", frames, 24, true);
				sprite.animations.play("123456");
				$.highlightGroup.add(sprite);
			}

			$.textDisplay.setText(currScene.text);

			var toWatch = null;
			if(!isUndefined(currScene.toMoveOn)) {
				toWatch = currScene.toMoveOn.project;
			}
			$.sceneChanged.dispatch(currScene.allowed, toWatch, false);

			//set listener for progressing to next scene
			var moveOnClick = isUndefined(currScene.toMoveOn);
			if(moveOnClick){
				$.watching = null;
			}
			$.nextButton.visible = moveOnClick;
		};

		this.onCurrSceneFinished = function() {
			var $ = this._(key);
			$.watching = null;

			$.currSceneIndex++;
			if($.currSceneIndex < $.introScenes.length) {
				this.displayCurrIntroScene();
			}
			else {
				this.onSkipClicked();
			}
		};

		this.onSkipClicked = function() {
			var $ = this._(key);
			$.isOn = false;
			this.clearScreen();
			this._(key).tutorialFinished.dispatch();
		};

		this.clearScreen = function() {
			var $ = this._(key);
			$.highlightGroup.removeAll();
			$.watching = null;
			$.game.world.removeChild($.skipButton);
			$.game.world.removeChild($.textDisplay);
			$.popupGroup.removeAll();
			$.game.world.removeChild($.popupGroup);
		};
	}

	TutorialHandler.prototype = {
		create: function() {
			TUTORIAL_START_ON = false;

			var $ = this._(key);
			if($.isOn) {
				$.popupGroup = $.game.add.group();
				$.popupGroup.x = TUT_BG_X;
				$.popupGroup.y = TUT_BG_Y;

				// create a new bitmap data object
			    var bmd = game.add.bitmapData(TUT_BG_WIDTH, TUT_BG_HEIGHT);

			    // draw to the canvas context like normal
			    bmd.ctx.beginPath();
			    bmd.ctx.rect(0, 0, TUT_BG_WIDTH, TUT_BG_HEIGHT);
			    bmd.ctx.fillStyle = GRAY_BG_COLOR;
			    bmd.ctx.fill();

			    // use the bitmap data as the texture for the sprite
			    // var sprite = game.add.sprite(200, 200, bmd);

				var popupBG = $.popupGroup.create(0, 0, bmd);//STILL_ATLAS_NAME, TUT_BG_NAME);
				$.textDisplay = $.game.add.text(TUT_TEXT_X, TUT_TEXT_Y,
					"", TUTORIAL_STYLE);
				$.popupGroup.add($.textDisplay);

				//create empty group to hold highlights
				$.highlightGroup = $.game.add.group();

				//center each button within a half of the bg
				var halfWidth = popupBG.width / 2;

				$.skipButton = $.game.add.button(SKIP_X, SKIP_Y, STILL_ATLAS_NAME,
					this.onSkipClicked, this,
					'buttonPopup', 'buttonPopup', 'buttonPopup', 'buttonPopup');
				$.popupGroup.add($.skipButton);
				labelButton($.skipButton, $.game, "SKIP TUTORIAL", TUTORIAL_STYLE);
				centerXWithin($.skipButton, halfWidth);
				$.skipButton.visible = false;

				$.nextButton = $.game.add.button(NEXT_X, NEXT_Y, STILL_ATLAS_NAME,
					this.onCurrSceneFinished, this,
					'buttonPopup', 'buttonPopup', 'buttonPopup', 'buttonPopup');
				labelButton($.nextButton, $.game, "NEXT", TUTORIAL_STYLE);
				$.popupGroup.add($.nextButton);
				$.nextButton.x = $.skipButton.x + halfWidth;

				this.displayCurrIntroScene();
			}
		},

		/**
		 * Check to see whether or not we should advance the scene
		 * Have prereqs of enabled project have been met?
		 */
		onTicked: function() {
			var $ = this._(key);
			if($.watching != null) {
				var prereqs = $.introScenes[$.currSceneIndex].toMoveOn;
				if($.watching[prereqs.key] == prereqs.value) {
					// console.log("Finished scene!");
					this.onCurrSceneFinished();
				}
			}
		},

		watchProject: function(project) {
			var $ = this._(key);
			this._(key).watching = project;
			// console.log("Watching " + project);
		},

		get isPaused(){
			var $ = this._(key);
			return $.introScenes[$.currSceneIndex].pauses;
		},
		get isOn(){ return this._(key).isOn; },

		get sceneChanged(){ return this._(key).sceneChanged; },
		get tutorialFinished(){ return this._(key).tutorialFinished; },
	};

	return TutorialHandler;
})();