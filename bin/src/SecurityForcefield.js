var FORCEFIELD_POS = {x: 100, y: 100};
var FORCEFIELD_PREFIX = "forceField/forceField_";
var FF_FRAMES = [
	{"start": 0, "end": 19},
	{"start": 20, "end": 59},
	{"start": 60, "end": 99},
	{"start": 100, "end": 139}
];
var FF_SPRITE_OFFSET_X = -12;
var FF_SPRITE_OFFSET_Y = -9;

// var FORCEFIELD_PREFIX = "animations/forcefield/forcefield1_";

SecurityForcefield = (function(){

	var key = {};

	function SecurityForcefield(gameParam) {
		var $ = {
			game: gameParam,
			sprite: undefined,
			securityLevel: 0,

			sprites: {
				"emails":[],
				"credentials":[],
				"creditCards":[],
			},
		};

		this._ = function(aKey) {
			if(aKey === key)
	        	return $;

	    	console.trace();
	      	throw new Error("The _() function must be called internally.");
		}
	}

	SecurityForcefield.prototype = {
		create: function(emailPositions, credentialPositions, creditCardPositions) {
			var positions = {
				"emails": emailPositions,
				"credentials": credentialPositions,
				"creditCards": creditCardPositions
			};

			this.createSprites("emails", positions);
			this.createSprites("credentials", positions);
			this.createSprites("creditCards", positions);
		},

		updateDisplay: function(securityLevel, numEmailServers, numCredentialServers, numCreditCardServers) {
			$ = this._(key);

			//set visibilities
			this.setServerVisibilities("emails", numEmailServers, securityLevel);
			this.setServerVisibilities("credentials", numCredentialServers, securityLevel);
			this.setServerVisibilities("creditCards", numCreditCardServers, securityLevel);

			// if(securityLevel != $.securityLevel) {
			// 	// console.log("EmailServers: " + numEmailServers);
			// 	// console.log("PW Servers: " + numCredentialServers);
			// 	// console.log("CC Servers: " + numCreditCardServers);
			// 	if(securityLevel > 0) {
			// 		this.playAnimation("emails", securityLevel)
			// 		$.sprite.animations.play('level' + securityLevel);
			// 	}
			// 	$.securityLevel = securityLevel;
			// }
		},

		setServerVisibilities: function(statName, numServers, securityLevel) {
			var $ = this._(key);
			for(var i = 0; i < $.sprites[statName].length; i++) {
				var sprite = $.sprites[statName][i];
				sprite.visible = i < numServers &&
					securityLevel > 0;
				if(sprite.visible) {
					var newAnimName = 'level' + securityLevel;
					var newAnim = sprite.animations.getAnimation(newAnimName);
					if(newAnim != sprite.animations.currentAnim){
						sprite.animations.play(newAnimName);
					}
				}
			}
		},

		createSprites: function(statName, positions) {
			var $ = this._(key);
			var numPositions = positions[statName].length;
			for(var i = 0; i < numPositions; i++) {

				var position = positions[statName][i];
				var sprite = $.game.add.sprite(
					position.x + FF_SPRITE_OFFSET_X,
					position.y + FF_SPRITE_OFFSET_Y,
					ANIM_ATLAS_NAME, FORCEFIELD_PREFIX + 130);
				this.addAnimation(sprite, 1);
				this.addAnimation(sprite, 2);
				this.addAnimation(sprite, 3);
				$.sprites[statName].push(sprite);
				sprite.visible = false;
			}
		},

		addAnimation: function(sprite, levelNumber) {
			var frames = Phaser.Animation.generateFrameNames(
					FORCEFIELD_PREFIX,
					FF_FRAMES[levelNumber].start,
					FF_FRAMES[levelNumber].end);
			sprite.animations.add('level' + levelNumber,
				frames,
				ANIM_FPS, true);
		}
	};

	return SecurityForcefield;
})();
